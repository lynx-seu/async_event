

all:
	@mkdir -p build
	@cd build 							&& \
		cmake ..  						&& \
		make							&& \
		./main

clean:
	rm -rf build

