
#pragma once

#include <set>
#include <tuple>
#include <list>
#include <map>
#include <string>
#include <memory>
#include <functional>
#include "future.h"
#include "dcs_proto.h"

namespace lynx {

class Connection;
class EventLoop;
class ZZContext;

class BaseTask
{
public:
    BaseTask(ZZContext *ctx): ctx_(ctx) {}
    virtual ~BaseTask() {}
    virtual bool handle(const dcs::Msg&) = 0;
    
    virtual Future<void> before_doing() = 0;
    virtual Future<void> after_doing() = 0;
    
    void write(const std::string& cmd);
protected:
    ZZContext *ctx_;
};

class DCSTask final
{
public:
    DCSTask(std::string task): id_(std::move(task)) {}
    ~DCSTask() {}

    std::string id() const {return id_;}
    int push(const dcs::TaskNode& node) {
        all_task_.push_back(node);
        return all_task_.size();
    }
    dcs::TaskNode current() const {
        assert(all_task_.size() > 0);
        return all_task_.back();
    }
    
    void set_completed() { finished_ = true; }
    bool is_completed() const { return finished_; }

private:
    const std::string        id_;
    bool                     finished_ {false};
    std::list<dcs::TaskNode> all_task_;
};

class ZZContext
{
public:
    using NEXT_TASK = std::function<void (const dcs::Msg&, const std::string&)>;
    using FUNC = std::function<void (const dcs::Msg&)>;
    using task_t = std::shared_ptr<BaseTask>;
    // 任务是一个2元组 (任务id, Msg)
    using task_id_t = DCSTask;

    explicit ZZContext(EventLoop *el, int fd);
    ~ZZContext();

    void set_material_state(FUNC cb) {on_material_state_ = std::move(cb);}
    void set_new_task(FUNC cb) {on_new_task_ = std::move(cb);}
    void set_next_task(NEXT_TASK cb) {on_next_task_ = std::move(cb);}
    void notify(const std::string& cmd);
    void notify(const dcs::Msg& msg);
    
    task_t spawn(const std::string& pos, dcs::TaskType type);
    void remove(task_t task);

    void destory();
    bool valid() const;
    EventLoop *get_loop() const;

private:
    size_t on_recv(Connection *, const char *, size_t);
    void on_close();
    void on_timer();
    void handle_msg(const dcs::Msg& msg);
    Future<void> exec_task(task_t task);
    void remove_task(task_t task) { to_remove_tasks_.push_back(task); }

private:
    Connection              *conn_;
    std::list<task_id_t>    all_task_ids_;
    FUNC                    on_new_task_;
    FUNC                    on_material_state_;
    NEXT_TASK               on_next_task_;
    long long               timer_id_;
    
    // helper for workflow
    std::list<task_t>       all_tasks_;
    std::list<task_t>       to_remove_tasks_;
};


} // lynx



