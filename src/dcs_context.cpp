
#include "dcs_context.h"
#include "eventloop.h"
#include "connection.h"
#include "logger.h"
#include "config.h"
#include <cassert>
#include <algorithm>

namespace lynx {

namespace statics {
    static const std::string dcs_header = "####";
    static const std::string dcs_footer = "****";
    static const int k_dcs_spin_delay =  1000;
    //static const int request_timeout = 10 * 1000;
}

static const char* strstr(const char* ptr, size_t len, 
                        const std::string& pattern)
{
    if (pattern.empty()) return nullptr;

    auto ret = std::search(ptr, ptr+len, pattern.begin(), pattern.end());
    return ret == ptr+len ? nullptr : ret;
}

static std::string make_arrive_cmd(dcs::MaterialType type) {
    char buf[4] = {'0', '0', '0', '0'};
    int o = dcs::get_material_order(type);
    if (o<4) buf[o] = '1';
    return "####" + std::string(buf, 4) + "****";
}

static std::string make_req_batch_cmd(dcs::MaterialType type) {
    char buf[4] = {'0', '0', '0', '0'};
    int o = dcs::get_material_order(type);
    if (o<4) buf[o] = '2';
    return "####" + std::string(buf, 4) + "****";
}

static std::string make_done_cmd(dcs::MaterialType type) {
    char buf[4] = {'0', '0', '0', '0'};
    int o = dcs::get_material_order(type);
    if (o<4) buf[o] = '3';
    return "####" + std::string(buf, 4) + "****";
}
    
// -------------------------- Pick Material -------------------------------

void BaseTask::write(const std::string& cmd) {if (ctx_) ctx_->notify(cmd);}

class PickMatTask: public BaseTask {
public:
    PickMatTask(ZZContext *ctx, dcs::MaterialType type):
        BaseTask(ctx), type_(type) {}
    ~PickMatTask() {}
    bool handle(const dcs::Msg& msg) {
        if (msg.is_batch()) {
            try {
                auto value = msg.string_value();
                auto type = (dcs::MaterialType)std::stoi(std::string(value, 0, 1));
                if (type_ == type) {
                    std::string batch(value, 1, 5);
                    batch_promise_.set_value();
                    batch_ = batch;
                    return true;
                }
            } catch (...) {
                LERROR("zz: Parse Batch Error");
            }
        }

        return false;
    }
    
    virtual Future<void> before_doing()  {
        write(make_arrive_cmd(type_));
        return lynx::make_ready_future();
    }
    
    virtual Future<void> after_doing() {
        write(make_req_batch_cmd(type_));
        if (!batch_.empty()) {
            return lynx::make_ready_future();
        }
        else {
            return batch_promise_.get_future();
        }
    }
    
private:
    const dcs::MaterialType type_;
    std::string             batch_;
    Promise<void>           batch_promise_;
};


class MainTask: public BaseTask {
public:
    MainTask(ZZContext *ctx, dcs::TaskNode node)
        :BaseTask(ctx), task_node_(node), type_(node.type) {}
    ~MainTask() {}
    bool handle(const dcs::Msg& msg) {
        if (msg.task_id() != task_node_.id) return false;
        
        if (!processing_) {
            processing_ = true;
            task_node_ = msg.tasknode_value();
            new_task_promise_.set_value();
        }
        else {
            LERROR("zz: main task processing, skip msg [%s]",
                   msg.dump().c_str());
        }
        return true;
    }
    
    virtual Future<void> before_doing()  {
        task_node_.set_state(dcs::DCS_ZZ_PROCESS);
        write(task_node_.dump());
        
        if (type_ == dcs::DELIVERY) {
            task_node_.set_state(dcs::DCS_ZZ_LOCATE_DONE);
            write(task_node_.dump());
    
            return wait_for_task(dcs::AGV_ZZ_ALLOW);
        }
        else {
            ctx_->get_loop()->after(statics::k_dcs_spin_delay, [=] {
                task_node_.set_state(dcs::DCS_ZZ_LOCATE_DONE);
                write(task_node_.dump());
            });
            return lynx::make_ready_future();
        }
    }

    virtual Future<void> after_doing() {
        task_node_.set_state(dcs::DCS_ZZ_DONE);
        write(task_node_.dump());
        // TODO: batch
        if (type_ == dcs::DELIVERY) {
        
        }
        return lynx::make_ready_future();
    }
private:
    Future<void> new_task_readiness() {
        processing_ = false;
        Promise<void> p;
        auto f = p.get_future();
        new_task_promise_ = std::move(p);
        return f;
    }
    
    Future<void> wait_for_task(dcs::TaskState state)
    {
        return new_task_readiness().then([=] {
            return task_node_.state == state ?
                        make_ready_future(): wait_for_task(state);
        });
    }
    
private:
    bool                    processing_ {true};
    Promise<void>           new_task_promise_;
    dcs::TaskNode           task_node_;
    const dcs::TaskType     type_;
};

//
//DeliveryTrayTask::DeliveryTrayTask(dcs::AgvPosition pos, dcs::MaterialType type,
//                    write_func f,
//                    agv_move_func move,
//                    agv_spin_func spin)
//     : BaseTask(std::move(f), std::move(move), std::move(spin))
//     , state_(NONE)
//     , pos_(pos)
//     , type_(type)
//{
//    assert(type == dcs::EMPTY_AL_BOX || type == dcs::EMPTY_COVER_BOARD);
//}
//
//DeliveryTrayTask::~DeliveryTrayTask() {}
//
//DeliveryTrayTask::State
//DeliveryTrayTask::change_state_to(DeliveryTrayTask::State state)
//{
//    if (state_ == state - 1) state_ = state;
//    return state_;
//}
//
//bool DeliveryTrayTask::handle(const dcs::Msg& msg)
//{
//    if (state_ == ARRIVED && msg.is_query()) {
//        try {
//            auto value = msg.string_value();
//            int o = dcs::get_material_order(type_);
//            if (value.at(o) == '2') {
//                state_ = ALLOW;
//                allow_promise_.set_value();
//                return true;
//            }
//        } catch (...) {
//            LERROR("zz: Parse Query Msg Error");
//        }
//    }
//
//    return false;
//}
//
//Future<bool> DeliveryTrayTask::exec(EventLoop *loop)
//{
//    auto f = lynx::make_ready_future().then([this] {
//        if (state_ == NONE)
//            return move(dcs::get_material_pos(type_), pos_);
//        return lynx::make_ready_future();
//    })
//    .then(*loop, [this] {
//        if (change_state_to(ARRIVED) == ARRIVED) {
//            write(make_arrive_cmd(type_));
//            return allow_promise_.get_future();
//        }
//        return lynx::make_ready_future();
//    }).then([this] {
//        if (change_state_to(ALLOW) == ALLOW) {
//            return spin(pos_, false);
//        }
//        return lynx::make_ready_future();
//    }).then(*loop, [this]() {
//        change_state_to(ALL_DONE);
//        write(make_done_cmd(type_));
//        return true;
//    });
//
//    return f;
//}
//

// -------------------------- ZZContext -----------------------------
ZZContext::ZZContext(EventLoop *el, int fd)
    : conn_(new Connection(el, fd))
{
    conn_->set_on_message([this](Connection *c, const char *data, size_t len) {
        return on_recv(c, data, len);
    });

    conn_->set_on_close([this](Connection *) {
        on_close();
    });

    timer_id_ = el->every(3000, [this](long) {
        on_timer();
    });
    
    notify("####00000000****");
}

ZZContext::~ZZContext()
{
    destory();
}

ZZContext::task_t ZZContext::spawn(const std::string& pos, dcs::TaskType type)
{
    if (CONFIG_INS->is_mat_pos(pos)) {
        auto task = std::make_shared<PickMatTask>(this, dcs::AL_BOX);
        all_tasks_.push_back(task);
        return task;
    }
    else if (CONFIG_INS->is_dcs_pos(pos)) {
        // find current task
        for (auto& t: all_task_ids_) {
            if (t.current().end_pos == pos) {
                if (t.current().type != type) {
                    LERROR("zz: dcs task type = %d, core task type = %d",
                            t.current().type, type);
                    return nullptr;
                }
                auto task = std::make_shared<MainTask>(this, t.current());
                all_tasks_.push_back(task);
                return task;
            }
        }
    }
    
    return nullptr;
}

void ZZContext::remove(ZZContext::task_t task)
{
    auto iter = std::find(all_tasks_.begin(), all_tasks_.end(), task);
    if (iter != all_tasks_.end()) {
        all_tasks_.erase(iter);
    }
}

void ZZContext::notify(const std::string& cmd)
{
    if (conn_) {
        conn_->write(cmd);
        if (cmd == "####0000****" || cmd == "####00000000****")
            LTRACE("zz: write <- %s", cmd.c_str());
        else
            LINFO("zz: write <- %s", cmd.c_str());
    }
}

void ZZContext::notify(const dcs::Msg& msg)
{
    notify(msg.dump());
}

EventLoop *ZZContext::get_loop() const
{
    return conn_ ? conn_->get_loop() : nullptr;
}

bool ZZContext::valid() const
{
    return conn_ ? conn_->valid() : false;
}

void ZZContext::destory()
{
    if (conn_) {
        conn_->get_loop()->del_timer_id(timer_id_);
        conn_->shutdown();
        delete conn_;
        conn_ = nullptr;
    }
}

void ZZContext::on_close()
{
    LWARN("zz: client closed!");
    destory();
}

void ZZContext::on_timer()
{
    static int count = 0;
    // heart
    notify("####00000000****");
    // 物料状态
    if (count++ % 3 == 0) notify("####0000****");
}

void ZZContext::handle_msg(const dcs::Msg& msg)
{
    // task complete
    if (msg.is_task_completed()) {
        auto pred = [=] (const task_id_t& t) {
            return t.id() == msg.task_id();
        };
        auto iter = std::find_if(all_task_ids_.begin(), all_task_ids_.end(), pred);
        if (iter != all_task_ids_.end()) {
            auto pos = iter->current().end_pos;
            if(on_next_task_) on_next_task_(msg, pos);
        }
        return;
    }

    // new task
    if (msg.is_task()) {
        auto task_node = msg.tasknode_value();
        
        auto pred = [=] (const task_id_t& t) {
            return t.id() == task_node.id;
        };
        auto iter = std::find_if(all_task_ids_.begin(), all_task_ids_.end(), pred);

        if (iter != all_task_ids_.end()) {
            if (iter->current().end_pos != task_node.end_pos) {
                auto pos = iter->current().end_pos;
                if(on_next_task_) on_next_task_(msg, pos);
                iter->push(task_node);
                return;
            }
        } else {
            task_id_t new_task(task_node.id);
            new_task.push(task_node);
            all_task_ids_.push_back(std::move(new_task));
            if (on_new_task_) on_new_task_(msg);
            return;
        }
    }

    // material state
    if (msg.is_query()) {
        auto v = msg.string_value();
        auto iter = std::find_if(v.begin(), v.end(), [](char p) {
            return p != '0' && p != '1';
        });
        if (iter == v.end()) {
            if(on_material_state_) on_material_state_(msg);
            return;
        }
    }
    
    // handle by task queue
    bool processed = false;
    for (auto task: all_tasks_) {
        if (task->handle(msg)) {
            processed = true;
            break;
        }
    }
    if (!processed) 
        LWARN("zz: msg [%s] hasn't been processed", msg.dump().c_str());

    // delete completed task
    for (auto task: to_remove_tasks_) {
        auto iter = std::find(all_tasks_.begin(), all_tasks_.end(), task);
        if (iter != all_tasks_.end())
            to_remove_tasks_.erase(iter);
    }
    to_remove_tasks_.clear();
}

size_t ZZContext::on_recv(Connection *, const char *data, size_t len)
{
    size_t cost = 0;
    auto begin = strstr(data, len, statics::dcs_header);
    auto end   = strstr(data, len, statics::dcs_footer);

    // skip
    if (!begin) {
        LWARN("zz: header not found, skip all: %s",
                std::string(data, len).c_str());
        cost = len;
    }
    else if (begin != data) {
        LWARN("zz: skip %s", std::string(data, begin-data).c_str());
        cost = begin - data;
    }

    // not found
    if (begin && end) {
        auto msg_size = end-begin+4;
        cost = end-data+4;

        std::string err;
        auto msg = dcs::Msg::parse(begin, msg_size, err);
        if (err.empty()) {
            LINFO("zz: read -> %s", std::string(begin, msg_size).c_str());
            handle_msg(msg);
        }
        else {
            LERROR("zz: msg [%s] error: %s",
                    std::string(begin, msg_size).c_str(),
                    err.c_str());
        }
    }

    return cost;
}

    
} // lynx

