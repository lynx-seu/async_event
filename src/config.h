
#pragma once

#include <string>
#include <map>

namespace lynx {

struct CorePos {
    std::string line;
    std::string name;
    std::string pos;
    
    bool operator==(const CorePos& other) const {
        bool x=  line == other.line && name == other.name &&
                pos == other.pos;
        return x;
    }
};

struct SockInfo {
    std::string ip;
    int         port;
};

class Config
{
public:
    static Config* Instance();

    bool init(const char* file);
    SockInfo get_server_info() const;
    SockInfo get_core_info() const;
    SockInfo get_dcs_info() const;

    bool to_mat(const CorePos&, std::string&) const;
    bool to_dcs(const CorePos&, std::string&) const;
    bool to_core(const std::string&, CorePos&) const;

    bool is_dcs_pos(const std::string& pos) const {
        return dcs_pos_map_.find(pos) != dcs_pos_map_.end();
    }
    bool is_mat_pos(const std::string& pos) const {
        return mat_pos_map_.find(pos) != mat_pos_map_.end();
    }
private:
    Config();
    ~Config();

private:
    std::map<std::string, SockInfo> sock_info_;
    std::map<std::string, CorePos>  dcs_pos_map_;
    std::map<std::string, CorePos>  mat_pos_map_;
};

}

#define CONFIG_INS lynx::Config::Instance()


