
#pragma once

#include "future.h"

namespace lynx {

class Agv
{
public:
    Agv(int id): id_(id) {}
    virtual ~Agv() {}

    // Agv上的机构 正反转
    virtual Future<void> process(bool) = 0;
    // 从当前点移动到目标位置
    virtual Future<void> move_to(const std::string& to) = 0;

    int id() const {return id_;}
private:
    int id_;
};

class ZZAgv: public Agv
{

};


} // lynx

