
#pragma once

#include <string>
#include <map>
#include <list>
#include <memory>
#include <functional>
#include "future.h"
#include "json11.hpp"

namespace lynx {

class EventLoop;
class Connection;

namespace dispatch {

// simple id generator
int id();

// request
struct Request {
    json11::Json request;
    Promise<void> promise;

    Request() { }
    Request(Request&& ) = default;
    Request& operator= (Request&& ) = default;
};
} // dispatch

class DispatchContext
{
public:
    using FUNC = std::function<void(json11::Json)>;

    DispatchContext(EventLoop *el, const std::string& ip, int port);
    ~DispatchContext();
    
    void set_on_pickup(FUNC cb) { on_pickup_ = std::move(cb); }
    void set_on_delivery(FUNC cb) { on_delivery_ = std::move(cb); }
    void set_on_query_task(FUNC cb) { on_query_task_ = std::move(cb); }

    void insert_batch(const std::string& pos, const std::string& batch) {
        batch_[pos] = batch;
    }
    Future<void> request(json11::Json req);
    Future<void> spin_at(json11::Json req);
private:
    void init();
    void write(const std::string& json);
    size_t on_recv(Connection *, const char* , size_t);
    void on_close(Connection *);
    void response_handshake(json11::Json r);
    void answer_batch(json11::Json query, const std::string& b);

    Future<void> request_impl(json11::Json req);

private:
    EventLoop *const    loop_;
    const std::string   ip_;
    const int           port_;
    std::shared_ptr<Connection>   conn_;
    std::list<dispatch::Request>  spin_pending_;
    std::list<dispatch::Request>  req_pending_;
    
    std::map<std::string, std::string> batch_;
    // callback
    FUNC on_pickup_;
    FUNC on_delivery_;
    FUNC on_query_task_;
};


} // lynx


