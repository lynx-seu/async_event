
#include "dcs_proto.h"
#include <sstream>
#include <iomanip>
#include <vector>
#include <list>
#include <map>
#include <climits>
#include <algorithm>
#include <assert.h>

namespace lynx {
namespace dcs {


int get_material_order(MaterialType type) {
    static const int material_order[] = {2, 0, 3, 1};
    return material_order[type];
}

std::string get_material_desc(MaterialType type)
{
    static const std::string material_desc_table[] = {
        "Al Box",
        "Cover Board",
        "Empty Al Box",
        "Empty Cover Board",
        "<null>",
        "<null>",
    };

    return material_desc_table[type];
}

namespace statics {
    static const size_t DCS_TASK_LEN       = 38;
    static const size_t TASK_COMPLETED_LEN = 20;
    static const size_t QUERY_LEN          = 12;
    static const size_t BATCH_LEN          = 14;
    static const size_t HEAD_FOOT_LEN      = 8;
    static const std::string DCS_HEADER = "####";
    static const std::string DCS_FOOTER = "****";
    static TaskNode null_task;
    static const std::string null_string = "";
} // statics

static bool check_header_footer(const std::string& header, const std::string& footer)
{
    return header == statics::DCS_HEADER && footer == statics::DCS_FOOTER;
}


// --------------------------- Msg ----------------------------

static bool check_msg_size(const std::string& content, bool with_head_foot = false)
{
    auto len = content.size();
    size_t complete_len = statics::TASK_COMPLETED_LEN;
    size_t query_len    = statics::QUERY_LEN;
    size_t batch_len    = statics::BATCH_LEN;

    if (!with_head_foot) {
        complete_len -= statics::HEAD_FOOT_LEN;
        query_len    -= statics::HEAD_FOOT_LEN;
        batch_len    -= statics::HEAD_FOOT_LEN;
    }
    return len == complete_len || len == query_len ||
            len == batch_len;
}

struct NullStruct {};
static void dump(NullStruct, std::string& out)
{
    out = "";
}

static void dump(const TaskNode& n, std::string& out)
{
    out = n.dump();
}

static void dump(const std::string& value, std::string& out)
{
    assert(check_msg_size(value, false));
    out = statics::DCS_HEADER + value + statics::DCS_FOOTER;
}

template <Msg::Type tag, typename T>
class Value: public MsgValue {
protected:
    explicit Value(const T& v): value_(v) {}
    explicit Value(T&& v): value_(std::move(v)) {}

    Msg::Type type() const {return tag;}
    const T value_;
    void dump(std::string& out) const {lynx::dcs::dump(value_, out);}
};

struct MsgNull final: public Value<Msg::NUL, NullStruct> {
    MsgNull(): Value({}) {}
};

class MsgCompleted final: public Value<Msg::TASK_COMPLETED, std::string> {
    const std::string& string_value() const {return value_;}
    const std::string& task_id() const {return task_;}
    std::string task_;
public:
    explicit MsgCompleted(const std::string& v): Value(v), task_(v, 0, 11) {}
    explicit MsgCompleted(std::string&& v): Value(std::move(v)), task_(v, 0, 11) {}
};

class MsgQuery final: public Value<Msg::QUERY, std::string> {
    const std::string& string_value() const {return value_;}
public:
    explicit MsgQuery(const std::string& v): Value(v) {}
    explicit MsgQuery(std::string&& v): Value(std::move(v)) {}
};

class MsgBatch final: public Value<Msg::BATCH, std::string> {
    const std::string& string_value() const {return value_;}
public:
    explicit MsgBatch(const std::string& v): Value(v) {}
    explicit MsgBatch(std::string&& v): Value(std::move(v)) {}
};

class MsgTask final: public Value<Msg::TASK, TaskNode> {
    const TaskNode& tasknode_value() const {return value_;}
    const std::string& task_id() const {return value_.id;}
public:
    explicit MsgTask(const TaskNode& v): Value(v) {}
    explicit MsgTask(TaskNode&& v): Value(std::move(v)) {}
};

const std::string& MsgValue::string_value() const {return statics::null_string;}
const TaskNode& MsgValue::tasknode_value() const  {return statics::null_task;}
const std::string&  MsgValue::task_id() const {return statics::null_string;}

Msg::Msg(): value_(std::make_shared<MsgNull>()) {}
Msg::Msg(const TaskNode& value): value_(std::make_shared<MsgTask>(value)) {}
Msg::Msg(const std::string& value) {
    auto len = value.size();

    if (len == statics::TASK_COMPLETED_LEN - statics::HEAD_FOOT_LEN) {
        value_ = std::make_shared<MsgCompleted>(value);
    }
    else if (len == statics::QUERY_LEN - statics::HEAD_FOOT_LEN) {
        value_ = std::make_shared<MsgQuery>(value);
    }
    else if (len == statics::BATCH_LEN - statics::HEAD_FOOT_LEN) {
        value_ = std::make_shared<MsgBatch>(value);
    }
    else {
        value_ = std::make_shared<MsgNull>();
    }
}

Msg::Type Msg::type() const {return value_->type();}

// getters
const std::string&  Msg::string_value() const   {return value_->string_value();}
const TaskNode&     Msg::tasknode_value() const {return value_->tasknode_value();}
const std::string&  Msg::task_id() const {return value_->task_id();}

// serialize
std::string Msg::dump() const {
    std::string out;
    value_->dump(out);
    return out;
}

Msg Msg::parse(const char *in, size_t len, std::string& err)
{
    if (in && len > 0) {
        auto str = std::string(in, len);
        if (len == statics::DCS_TASK_LEN) {
            auto n = TaskNode::parse(str, err);
            return Msg(n);
        }
        else if (check_msg_size(str, true)) {
            return Msg(std::string(in+4, len-8));
        }
    }

    err = "error msg";
    return Msg();
}

    
// --------------------------- TaskNode ----------------------------
bool TaskNode::operator==(const TaskNode& n) const
{
    return id == n.id && start_pos == n.start_pos &&
            type == n.type && material == n.material;
}
    
bool TaskNode::operator!=(const TaskNode& n) const
{
    return !(*this == n);
}
    
std::string TaskNode::dump() const
{
    std::ostringstream oss;
    oss << statics::DCS_HEADER
        << id
        << std::setw(1) << (int)type
        << start_pos
        << end_pos
        << std::setw(1) <<  (int)material
        << batch_number
        << std::setw(1) << (int)state
        << std::setw(1) << (int)seq
        << statics::DCS_FOOTER;
    return oss.str();
}

TaskNode TaskNode::parse(const std::string& in, std::string& err)
{
    if (in.size() != statics::DCS_TASK_LEN) {
        err = "task command length error";
        return statics::null_task;
    }
    // 报文头尾
    auto header = std::string(in, 0, 4);
    auto footer = std::string(in, 34, 4);
    if (!check_header_footer(header, footer)) {
        err = "task command header or footer error";
        return statics::null_task;
    }

    // 防止 stoi 转换失败导致crash
    TaskNode task_node;
    try {
        task_node = TaskNode {
            std::string(in, 4, 11),                           // id
            std::string(in, 16, 5),                           // start pos
            std::string(in, 21, 5),                           // end pos
            std::string(in, 27, 5),                           // batch number
            std::stoi(std::string(in, 33, 1)),                // seq
            (TaskType)std::stoi(std::string(in, 15, 1)),      // type
            (TaskState)std::stoi(std::string(in, 32, 1)),     // state
            (MaterialType)std::stoi(std::string(in, 26, 1)),  // material
        };
    } catch(const std::exception& e) {
        err = e.what();
    }
    
    return task_node;
}

// ---------------------------TaskMgr----------------------------
struct AgvInfo {
    std::string task_id                 = statics::null_string;
    // 用于标识这辆小车是放什么材料的, 避免左右位置放置不同材料
    // NOTICE: type 不包含空托盘, 空托盘和满载都用满载表示
    // 例: 车上放有 AL_BOX/EMPTY_AL_BOX 时, 小车类型为AL_BOX, 此时小车不能放入其他材料
    MaterialType type                   = MATERIAL_NONE;
    MaterialType material[MAX_POSITION] = {MATERIAL_NONE, MATERIAL_NONE};
    std::string  batch[MAX_POSITION]    = {statics::null_string};
};

struct ZZTaskMgr::Impl {
    using task_t = std::list<TaskNode>;
    // 物料区 状态
    bool material[MAX_MATERIAL];
    // 记录Agv对应的任务号, 车上的货物类型, 以及货物对应的批号
    std::map<int, AgvInfo> agv_info;
    // 所有的任务 (id, task queue)
    std::map<std::string, task_t> tasks;
    
    // helper
    void check_agv(int agv) {
        if (agv_info.find(agv) == agv_info.end()) {
            agv_info.emplace(agv, AgvInfo());
        }
    }
    
    void add_task(const TaskNode &task) {
        if (tasks.find(task.id) == tasks.end()) {
            tasks.emplace(task.id, task_t());
        }
        auto& task_queue = tasks.at(task.id);
        if (task_queue.back() != task) {
            task_queue.push_back(task);
        }
    }
};


ZZTaskMgr::ZZTaskMgr() :pimpl_(new Impl) {}
ZZTaskMgr::~ZZTaskMgr() {}
    
void ZZTaskMgr::add_task(const TaskNode &task)
{
    pimpl_->add_task(task);
}
    
bool ZZTaskMgr::find_a_task(TaskNode& task) const
{
    for (const auto& kv: pimpl_->tasks) {
        if (!task_is_processing(kv.first)) {
            task = kv.second.back();
            return true;
        }
    }
    return false;
}
    
bool ZZTaskMgr::task_is_processing(const std::string& task) const
{
    for (const auto& kv: pimpl_->agv_info) {
        if (kv.second.task_id == task) return true;
    }
    return false;
}
    
    
bool ZZTaskMgr::contain_task(const TaskNode& task) const
{
    const auto& tasks = pimpl_->tasks;
    if (tasks.find(task.id) != tasks.end()) {
        const auto& task_queue = tasks.at(task.id);
        return std::find(task_queue.begin(), task_queue.end(), task)
                    != task_queue.end();
    }

    return false;
}
    
void ZZTaskMgr::finish_task(const std::string& id)
{
    auto& tasks = pimpl_->tasks;
    if (tasks.find(id) != tasks.end()) tasks.erase(id);
    
    for (const auto& kv: pimpl_->agv_info) {
        if (kv.second.task_id == id) {
            assign(kv.first, "");
        }
    }
}
    

int ZZTaskMgr::find_agv(const TaskNode& task, const distance_func& distance) const
{
    const TaskType     type = task.type;
    const MaterialType material = task.material;

    // first, 查找是否有相同任务号的agv, 存在直接返回
    for (const auto& kv: pimpl_->agv_info) {
        if (kv.second.task_id == task.id) return kv.first;
    }
    
    int agv_satisfied = -1;
    float min_distance = static_cast<float>(LLONG_MAX);
    // second, 如果是送料, 判断车上是否已经有料, 否则需要取料
    if (type == DELIVERY) {
        for(const auto& kv: pimpl_->agv_info) {
            const auto& agv = kv.second;
            const auto& to_pos = task.end_pos;
            bool with_material = (agv.material[AGV_LEFT] == material ||
                                  agv.material[AGV_RIGHT] == material);
            bool without_task = (agv.task_id == "");
    
            if (without_task && with_material) {
                int dist = distance(kv.first, to_pos);
                if (dist < min_distance) {
                    min_distance = dist;
                    agv_satisfied = kv.first;
                }
            }
        }
    }

    // third, 取料或者取空托盘
    for(const auto& kv: pimpl_->agv_info) {
        const auto& agv = kv.second;
        // TODO: fix 取料的位置
        const auto& to_pos = type == PICK_UP ? task.end_pos : task.start_pos;
        bool can_take_material = (agv.type == material || agv.type == MATERIAL_NONE);
        bool without_material = (agv.material[AGV_LEFT] == MATERIAL_NONE ||
                                 agv.material[AGV_RIGHT] == MATERIAL_NONE);
        bool without_task = (agv.task_id == "");
        
        if (without_task && can_take_material && without_material)  {
            int dist = distance(kv.first, to_pos);
            if (dist < min_distance) {
                min_distance = dist;
                agv_satisfied = kv.first;
            }
        }
    }

    return agv_satisfied;
}
    
void ZZTaskMgr::update_material(const std::string& str)
{
    assert(str.size() == 4);
    pimpl_->material[AL_BOX]            = (bool)(str.at(0) - '0');
    pimpl_->material[EMPTY_AL_BOX]      = (bool)(str.at(1) - '0');
    pimpl_->material[COVER_BOARD]       = (bool)(str.at(2) - '0');
    pimpl_->material[EMPTY_COVER_BOARD] = (bool)(str.at(3) - '0');
}
    
bool ZZTaskMgr::exist_material(MaterialType type) const
{
    assert(type < MAX_MATERIAL);
    return pimpl_->material[type];
}
    
std::string ZZTaskMgr::dump() const
{
    std::ostringstream oss;
    oss << "Agv State: " << std::endl;
    for (const auto& kv:  pimpl_->agv_info) {
        const auto& info = kv.second;
        oss << "\tagv: " << kv.first
            << "\ttask: " << (info.task_id == "" ? "<null>" : info.task_id)
            << "\tleft: " << get_material_desc(info.material[AGV_LEFT]);
        if (info.batch[AGV_LEFT] != "")
            oss << "(" << info.batch[AGV_LEFT] << ")";
        
        oss << "\tright: " << get_material_desc(info.material[AGV_RIGHT]);
        if (info.batch[AGV_RIGHT] != "")
            oss << "(" << info.batch[AGV_RIGHT] << ")";
        oss << std::endl;
    }
    
    oss << "Material Info: " << std::endl;
    for (auto i=0; i<MAX_MATERIAL; i++) {
        oss << "\t" << (exist_material((MaterialType)i) ? "available" : "unavailable");
    }
    oss << std::endl;

    if (pimpl_->tasks.size()>0) oss << "Task state: " << std::endl;
    for (const auto& kv: pimpl_->tasks) {
        oss << "\ttask: " << kv.first;
        std::string sep = "\tseq: (";
        for (const auto& task: kv.second) {
            oss << sep << task.seq;
            sep = ", ";
        }
        oss << ")" << std::endl;
    }

    return oss.str();
}
    
void ZZTaskMgr::assign(int agv, const std::string& id)
{
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info;
    info.at(agv).task_id = id;
}

std::string ZZTaskMgr::get_task_id(int agv) const
{
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info;
    return info.at(agv).task_id;
}
  
void ZZTaskMgr::assign(int agv, AgvPosition position, MaterialType type)
{
    // 维护车上的位置
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info.at(agv);

    // 清空位置信息, 如果所有位置都为空, 车运输类型改变
    if (type == MATERIAL_NONE) {
        info.material[position] = type;
        if (info.material[AGV_LEFT] == MATERIAL_NONE &&
            info.material[AGV_RIGHT] == MATERIAL_NONE)
            info.type = type;
    }
    else {
        assert(info.type == type || info.type == MATERIAL_NONE);
        info.type = type;
        info.material[position] = type;
    }
}

MaterialType ZZTaskMgr::get_material(int agv, AgvPosition position)
{
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info;
    return info.at(agv).material[position];
}
                  
void ZZTaskMgr::assign(int agv, AgvPosition position, const std::string& batch)
{
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info;
    info.at(agv).batch[position] = batch;
}

std::string ZZTaskMgr::get_batch(int agv, AgvPosition position)
{
    pimpl_->check_agv(agv);
    auto& info = pimpl_->agv_info;
    return info.at(agv).batch[position];
}
    



} // dcs
} // lynx

