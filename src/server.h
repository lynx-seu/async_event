
#pragma once

#include <memory>
#include "json11.hpp"
#include "config.h"
#include "dcs_proto.h"

namespace lynx {

class EventLoop;
class ZZContext;
class DispatchContext;

// 1. 连接调度获取小车信息
// 2. 向dcs提供服务
// 3. 数据保存(用于现场恢复)
class Server final
{
public:
    Server(EventLoop *el, const char* ip, int port);
    ~Server();

    void connect(const char *ip, int port);
private:
    void on_zz_material_state(const dcs::Msg&);
    void on_zz_new_task(const dcs::Msg&);
    void on_zz_next_task(const dcs::Msg&, const std::string& pre);
    void on_core_task(json11::Json, dcs::TaskType);
    void on_core_query_task(json11::Json);
    void on_timer();
    void on_accept(int fd);
    void destory();
    void answer_by(const dcs::Msg& msg, const CorePos& pos);
private:
    EventLoop   *loop_;
    int         fd_;
    long        cron_id_;

    std::list<json11::Json> query_queue_;
    // 组装区context
    std::unique_ptr<ZZContext> zz_ctx_;
    // 调度 context
    std::unique_ptr<DispatchContext> dispatch_ctx_;
};

} // lynx

