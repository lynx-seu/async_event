
#include <fstream>
#include <sstream>
#include "config.h"
#include "json11.hpp"

namespace lynx {

static SockInfo parse_sock_info(json11::Json j)
{
    SockInfo s {
        "127.0.0.1", 0
    };
    auto ip = j[0].string_value();
    if (ip != "") s.ip = ip;
    s.port = j[1].int_value();
    
    return s;
}

Config* Config::Instance()
{
    static Config c;
    return &c;
}

Config::Config() {}
Config::~Config() {}


bool Config::init(const char* file)
{
    std::ifstream s(file, std::ios::binary | std::ios::in);
    if (!s.is_open()) return false;

    std::ostringstream buffer;
    buffer << s.rdbuf();
    std::string err;
    auto v = json11::Json::parse(buffer.str(), err);
    if (!err.empty()) return false;
    const auto& common  = v["common"];
    const auto& pos_map = v["dcs_pos_map"].object_items();
    const auto& mat_map = v["mat_pos_map"].object_items();

    // common
    sock_info_["dcs"] = parse_sock_info(common["dcs"]);
    sock_info_["server"] = parse_sock_info(common["server"]);
    sock_info_["core"] = parse_sock_info(common["dispatch_core"]);

    // dcs pos map
    for (auto v: pos_map) {
        CorePos p {
            v.second[0].string_value(),
            v.second[1].string_value(),
            v.second[2].string_value(),
        };
        dcs_pos_map_.emplace(v.first, std::move(p));
    }
    
    // mat pos map
    for (auto v: mat_map) {
        CorePos p {
            v.second[0].string_value(),
            v.second[1].string_value(),
            v.second[2].string_value(),
        };
        dcs_pos_map_.emplace(v.first, std::move(p));
    }

    return true;
}

SockInfo Config::get_server_info() const
{
    if (sock_info_.find("server") != sock_info_.end())
        return sock_info_.at("server");
    else
        return SockInfo {"127.0.0.1", 8999};
}

SockInfo Config::get_core_info() const
{
    if (sock_info_.find("core") != sock_info_.end())
        return sock_info_.at("core");
    else
        return SockInfo {"127.0.0.1", 2000};
}

SockInfo Config::get_dcs_info() const
{
    if (sock_info_.find("dcs") != sock_info_.end())
        return sock_info_.at("dcs");
    else
        return SockInfo {"127.0.0.1", 0};
}

bool Config::to_mat(const CorePos& core, std::string& dcs) const
{
    for (const auto& k: mat_pos_map_) {
        if (k.second == core) {
            dcs = k.first;
            return true;
        }
    }
    return false;
}

bool Config::to_dcs(const CorePos& core, std::string& dcs) const
{
    for (const auto& k: dcs_pos_map_) {
        if (k.second == core) {
            dcs = k.first;
            return true;
        }
    }
    return false;
}

bool Config::to_core(const std::string& dcs, CorePos& core) const
{
    auto& map = is_mat_pos(dcs) ? mat_pos_map_ : dcs_pos_map_;
    for (const auto& k: map) {
        if (k.first == dcs) {
            core = k.second;
            return true;
        }
    }
    return false;
}

} // lynx

