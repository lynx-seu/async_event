
#include <functional>
#include <algorithm>
#include "dispatch_context.h"
#include "logger.h"
#include "eventloop.h"
#include "connection.h"
#include "threadpool.h"
#include "net.h"
#include "config.h"

namespace lynx {

#define CRLF "\r\n"

static const char* strstr(const char* ptr, size_t nBytes, const char* pattern, size_t nBytes2) {
    if (!pattern || *pattern == 0)
        return nullptr;

    const char* ret = std::search(ptr, ptr + nBytes, pattern, pattern + nBytes2);
    return  ret == ptr + nBytes ? nullptr : ret;
}

static const char* search_crlf(const char* ptr, size_t nBytes) {
    return strstr(ptr, nBytes, CRLF, 2);
}

namespace dispatch {

int id()
{
    static int v = 1;
    return v++;
}

} // dispatch


DispatchContext::DispatchContext(EventLoop *el, const std::string& ip, int port)
    : loop_(el), ip_(ip), port_(port)
    , conn_(nullptr)
{
    init();
}

DispatchContext::~DispatchContext()
{
}

Future<void> DispatchContext::request(json11::Json req)
{
    auto timeout_cb = [this, req](Try<void>&& v) {
        if (v.has_exception()) {
            for(auto iter=req_pending_.begin(); iter!=req_pending_.end(); ++iter) {
                if (iter->request == req) {
                    req_pending_.erase(iter);
                    break;
                }
            }
            return request(req);
        }
        return lynx::make_ready_future();
    };
    auto f = request_impl(std::move(req));
    return f.timeout(*loop_, 5 * 1000).then(timeout_cb);
}

Future<void> DispatchContext::spin_at(json11::Json req)
{
    response_handshake(req);
    
    dispatch::Request pending;
    auto f = pending.promise.get_future();
    pending.request = req;
    spin_pending_.push_back(std::move(pending));
    return f;
}

Future<void> DispatchContext::request_impl(json11::Json v)
{
    write(v.dump());
    dispatch::Request req;
    auto f = req.promise.get_future();
    req.request = v;
    req_pending_.push_back(std::move(req));
    return f;
}

void DispatchContext::init()
{
    auto fd = lynx::net::connect_nonblock(ip_.c_str(), port_);
    if (fd.has_value()) {
        LINFO("Dispatch: connecting to dispatch core");
        conn_ = std::make_shared<Connection>(loop_, fd);
        conn_->set_on_close([this](Connection *c){
            on_close(c);
        });
        conn_->set_on_message([this](Connection *c, const char *data, size_t len) {
            return on_recv(c, data, len);
        });
    }
    else {
        on_close(nullptr);
    }
}

void DispatchContext::write(const std::string& json)
{
    if (conn_->valid()) {
        auto j = json + CRLF;
        conn_->write(j.c_str(), j.size());
    }
}

void DispatchContext::response_handshake(json11::Json r)
{
    json11::Json resp = json11::Json::object {
        {"Dev_Type", "MES"},
        {"Dev_ID",   1},
        {"Dev_Data", json11::Json::object {
            {"TimeStamp_sec",   r["Dev_Data"]["TimeStamp_sec"]},
            {"TimeStamp_usec",  r["Dev_Data"]["TimeStamp_usec"]},
            {"Comm_Type",       "HandShake"},
            {"HandShake_ID",    r["Dev_Data"]["Comm_ID"]},
            {"Task_ID",         r["Dev_Data"]["Task_ID"]},
        }},
    };
    write(resp.dump());
}

void DispatchContext::answer_batch(json11::Json q, const std::string& b)
{
    json11::Json resp = json11::Json::object {
        {"Dev_Type", "MES"},
        {"Dev_ID",   1},
        {"Dev_Data", json11::Json::object {
            {"TimeStamp_sec",   q["Dev_Data"]["TimeStamp_sec"]},
            {"TimeStamp_usec",  q["Dev_Data"]["TimeStamp_usec"]},
            {"Comm_Type",       "Answer"},
            {"HandShake_ID",    q["Dev_Data"]["Comm_ID"]},
            {"Task_ID",         q["Dev_Data"]["Task_ID"]},
            {"Batch",           b},
            {"Line_Name",       q["Dev_Data"]["Comm_LineName"]},
            {"Pos_Name",        q["Dev_Data"]["Comm_PosName"]},
            {"Sta_Name",        q["Dev_Data"]["Comm_MatName"]},
        }},
    };
    write(resp.dump());
}

size_t DispatchContext::on_recv(Connection *, const char* data, size_t len)
{
    // command - handshake
    auto s = search_crlf(data, len);
    if (!s) return 0;
    
    auto cost = s - data + 2;
    std::string content(data, cost-2);
    std::string err;
    
    auto json = json11::Json::parse(content, err);
    if (!err.empty()) {
        LERROR("Dispatch: Json [%s] err: %s", content.c_str(), err.c_str());
        return cost;
    }
    
    auto type = json["Dev_Data"]["Comm_Type"].string_value();
    if (type == "Command") {
        auto t = json["Dev_Data"]["Comm_ProType"].string_value();
        
        if (t == "Load") {
            on_pickup_(json);
        }
        else if (t == "Unload") {
            on_delivery_(json);
        }
        else if (t == "Over") {
            auto& l = spin_pending_;
            
            for (auto iter = l.begin(); iter!=l.end(); ++iter) {
                auto d1 = iter->request["Dev_Data"];
                auto d2 = json["Dev_Data"];
                if (d1["Comm_LineName"] == d2["Comm_LineName"] ||
                    d1["Comm_PosName"] == d2["Comm_PosName"] ||
                    d1["Comm_MatName"] == d2["Comm_MatName"]) {
                    response_handshake(json);
                    iter->promise.set_value();
                    l.erase(iter);
                    break;
                }
            }
        }
        else {
            LERROR("Dispatch: Command request with a error protype[%s]", t.c_str());
        }
    }
    else if (type == "HandShake") {
        // 我唯一主动发的命令, 收回handshake结束
        auto id = json["Dev_Data"]["HandShake_ID"];
        auto pred = [id](const dispatch::Request& req) {
            const auto& j = req.request;
            return id.int_value() == j["Dev_Data"]["Comm_ID"].int_value();
        };
        
        if (id.is_number()) {
            auto iter = std::find_if(req_pending_.begin(), req_pending_.end(), pred);
            if (iter != req_pending_.end()) {
                iter->promise.set_value();
                req_pending_.erase(iter);
            }
        }
    }
    else if (type == "Query") {
        auto& data = json["Dev_Data"];
        auto query = json["Dev_Data"]["Query"].string_value();
        
        if (query == "Batch") {
            auto line = data["Comm_LineName"].string_value();
            auto name = data["Comm_MatName"].string_value();
            auto pos = data["Comm_PosName"].string_value();
            CorePos cp {line, name, pos};
            std::string x;
            if (CONFIG_INS->to_dcs(cp, x)) {
                if (batch_.find(x) != batch_.end()) {
                    answer_batch(json, batch_.at(x));
                    batch_.erase(batch_.find(x));
                }
                else {
                    loop_->every(1000, [this, x, json](long id) {
                        if (batch_.find(x) != batch_.end()) {
                            answer_batch(json, batch_.at(x));
                            batch_.erase(batch_.find(x));
                            loop_->del_timer_id(id);
                        }
                    });
                }
            }
        }
        else if (query == "NextTask") {
            if (on_query_task_) on_query_task_(json);
        }
        else {
            LERROR("Dispatch: Query with a error type[%s]", query.c_str());
        }
    }
    else {
        LERROR("Dispatch: Json [%s] with type [%s]", content.c_str(), type.c_str());
    }
    
    return cost;
}

void DispatchContext::on_close(Connection *c)
{
    LWARN("Dispatch: connection close, reconnect after 5s ...");
    loop_->after(5*1000, [this] { init(); });
}


} // lynx

