#pragma once

#include <cstdio>
#include <iostream>
#include <ctime>
#include <type_traits>

#if defined(__APPLE__)
    #define TRACE_COLOR     "📣"
    #define DEBUG_COLOR     "🐞"
    #define INFO_COLOR      "ℹ️"
    #define WARN_COLOR      "⚠️"
    #define ERROR_COLOR     "❗"
#elif defined(__WIN32__)
    #define TRACE_COLOR     "T"
    #define DEBUG_COLOR     "D"
    #define INFO_COLOR      "I"
    #define WARN_COLOR      "W"
    #define ERROR_COLOR     "E"
#else
    #define TRACE_COLOR     "\33[34mT\33[0m"
    #define DEBUG_COLOR     "\33[36mD\33[0m"
    #define INFO_COLOR      "\33[32mI\33[0m"
    #define WARN_COLOR      "\33[33mW\33[0m"
    #define ERROR_COLOR     "\33[31mE\33[0m"
#endif

namespace priv {

enum Level {
    TRACE = 0,
    DEBUG,
    INFO,
    WARN,
    ERROR,
};

static const char *level_desc[] = {
    TRACE_COLOR,
    DEBUG_COLOR,
    INFO_COLOR,
    WARN_COLOR,
    ERROR_COLOR,
};

static Level g_level = DEBUG;

template <class...Args>
typename std::enable_if<sizeof...(Args)!=0, void>::type
format(char *buf, int size, const char *fmt, Args... args)
{
    snprintf(buf, size, fmt, std::forward<Args>(args)...);
}

template <class...Args>
typename std::enable_if<sizeof...(Args)==0, void>::type
format(char *buf, int size, const char *fmt, Args... args)
{
    snprintf(buf, size, "%s", fmt);
}

template <class... Args>
void log(Level level, const char* fmt, Args... args)
{
    if(level < g_level) return;
    char buf[1024];
    std::cout << " " << level_desc[level] << " ";
    auto now = std::time(nullptr);
    auto r = std::strftime(buf, 1024, "<%H:%M:%S>", std::localtime(&now));
    if (r) std::cout << buf << " ";
    
    format(buf, sizeof(buf), fmt, std::forward<Args>(args)...);
    std::cout << buf << std::endl;
}

} // priv

#define LTRACE(...)         priv::log(priv::TRACE, __VA_ARGS__)
#define LDEBUG(...)         priv::log(priv::DEBUG, __VA_ARGS__)
#define LINFO(...)          priv::log(priv::INFO,  __VA_ARGS__)
#define LWARN(...)          priv::log(priv::WARN,  __VA_ARGS__)
#define LERROR(...)         priv::log(priv::ERROR, __VA_ARGS__)

