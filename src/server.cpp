
#include "server.h"
#include "logger.h"
#include "dcs_context.h"
#include "dispatch_context.h"
#include "eventloop.h"
#include "net.h"
#include "future.h"
#include "json11.hpp"
#include "config.h"

namespace lynx {

static Future<void> agv_move_to(const std::string& pos,
                                dcs::AgvPosition offset)
{
    (void)pos;
    (void)offset;
    return lynx::make_ready_future();
}

Server::Server(EventLoop *el, const char* ip, int port)
    : loop_(el), fd_(lynx::net::server(ip, port))
    , cron_id_(-1)
{
    LINFO("server: start");
    loop_->async_read(fd_, std::bind(&Server::on_accept, this, std::placeholders::_1));
//    cron_id_ = loop_->every(1000, [this](long) {on_timer();});
}

Server::~Server()
{
    LINFO("server: stop");
    destory();
}

void Server::destory()
{
    net::close(fd_);
    loop_->del_timer_id(cron_id_);
    loop_->del_async_read_fn(fd_);
}

void Server::connect(const char *ip, int port)
{
    auto p = std::make_unique<DispatchContext>(loop_, ip, port);
    auto f1 = std::bind(&Server::on_core_task, this, std::placeholders::_1, dcs::PICK_UP);
    p->set_on_pickup(f1);
    auto f2 = std::bind(&Server::on_core_task, this, std::placeholders::_1, dcs::DELIVERY);
    p->set_on_delivery(f2);
    auto f3 = std::bind(&Server::on_core_query_task, this, std::placeholders::_1);
    p->set_on_query_task(f3);
    
    dispatch_ctx_ = std::move(p);
}

void Server::on_zz_material_state(const dcs::Msg&)
{
}

void Server::on_zz_new_task(const dcs::Msg& msg)
{
    if (!zz_ctx_ || !dispatch_ctx_) return;
    auto task = msg.tasknode_value();
    
    // 转换位置
    CorePos core_pos;
    if (!CONFIG_INS->to_core(task.end_pos, core_pos)) {
        LERROR("Server: pos [%s] doesn't exist!!!", task.end_pos.c_str());
        return;
    }

    // 新任务
    json11::Json json = json11::Json::object {
        {"Dev_Type", "MES"},
        {"Dev_ID", 1},
        {"Dev_Data", json11::Json::object {
            {"Comm_Type", "Command"},
            {"Comm_ID", dispatch::id()},
            {"Line_Name", core_pos.line},
            {"Pos_Name", core_pos.pos},
            {"Sta_Name", core_pos.name},
        }},
    };
    dispatch_ctx_->request(json);
}

void Server::on_zz_next_task(const dcs::Msg& msg, const std::string& pre_pos)
{
    // 转换位置
    CorePos core_pos;
    if (!CONFIG_INS->to_core(pre_pos, core_pos)) {
        LERROR("Server: pos [%s] doesn't exist!!!", pre_pos.c_str());
        return;
    }
    
    answer_by(msg, core_pos);
}

void Server::answer_by(const dcs::Msg& msg, const CorePos& pos)
{
    auto pred = [=](json11::Json v) {
        auto data = v["Dev_Data"];
        return data["Comm_LineName"].string_value() == pos.line &&
                data["Comm_PosName"].string_value() == pos.pos &&
                data["Comm_StaName"].string_value() == pos.name;
    };
    
    auto iter = std::find_if(query_queue_.begin(), query_queue_.end(), pred);
    
    if (iter != query_queue_.end()) {
        json11::Json json;
        
        if (msg.is_task_completed()) {
            json = json11::Json::object {
                {"Dev_Type", "MES"},
                {"Dev_ID", 1},
                {"Dev_Data", json11::Json::object {
                    {"TimeStamp_sec",   (*iter)["Dev_Data"]["TimeStamp_sec"]},
                    {"TimeStamp_usec",  (*iter)["Dev_Data"]["TimeStamp_usec"]},
                    {"Comm_Type",       "Answer"},
                    {"HandShake_ID",    (*iter)["Dev_Data"]["Comm_ID"]},
                    {"Task_ID",         (*iter)["Dev_Data"]["Task_ID"]},
                }},
            };
        }
        else if (msg.is_task()) {
            auto to_pos =  msg.tasknode_value().end_pos;
            CorePos core_pos;
            if (!CONFIG_INS->to_core(to_pos, core_pos)) {
                LERROR("Server: No dcs pos [%s]", to_pos.c_str());
                return;
            }
            
            json = json11::Json::object {
                {"Dev_Type", "MES"},
                {"Dev_ID", 1},
                {"Dev_Data", json11::Json::object {
                    {"TimeStamp_sec",   (*iter)["Dev_Data"]["TimeStamp_sec"]},
                    {"TimeStamp_usec",  (*iter)["Dev_Data"]["TimeStamp_usec"]},
                    {"Comm_Type",       "Answer"},
                    {"HandShake_ID",    (*iter)["Dev_Data"]["Comm_ID"]},
                    {"Task_ID",         (*iter)["Dev_Data"]["Task_ID"]},
                    {"Line_Name",       core_pos.line},
                    {"Pos_Name",        core_pos.pos},
                    {"Sta_Name",        core_pos.name},
                }},
            };
        }
        else {
            LERROR("Server: error next task: %s", msg.dump().c_str());
        }
    
        dispatch_ctx_->request(json);
    }
    else {
        loop_->after(1000, [=] { answer_by(msg, pos); });
    }
}

void Server::on_core_query_task(json11::Json json)
{
    query_queue_.push_back(json);
}

void Server::on_core_task(json11::Json js, dcs::TaskType type)
{
    auto line = js["Dev_Data"]["Comm_LineName"].string_value();
    auto name = js["Dev_Data"]["Comm_MatName"].string_value();
    auto pos = js["Dev_Data"]["Comm_PosName"].string_value();
    CorePos cp {line, name, pos};
    std::string dcs_pos;
    if (!CONFIG_INS->to_dcs(cp, dcs_pos)) {
        LERROR("Server: error position, line: %s, name: %s, pos: %s",
                line.c_str(), name.c_str(), pos.c_str());
        return;
    }
    auto task = zz_ctx_->spawn(dcs_pos, type);
    if (!task) return;

    task->before_doing().then([=] {
        return dispatch_ctx_->spin_at(js);
    })
    .then([=] {
        return task->after_doing();
    }).then([=] {
        zz_ctx_->remove(task);
    });
}

void Server::on_timer()
{
    LTRACE("server: cron job!");
}

void Server::on_accept(int fd)
{
    auto x = lynx::net::accept(fd);
    if (x.has_exception()) {
        LERROR("server: accept error");
        return;
    }
    
    LINFO("server: accept ip = %s, port = %d, fd = %d",
            x.value().ip.c_str(), x.value().port, x.value().fd);
    auto p = std::make_unique<ZZContext>(loop_, x.value().fd);
    zz_ctx_ = std::move(p);
    
    zz_ctx_->set_new_task(std::bind(&Server::on_zz_new_task, this, std::placeholders::_1));
    auto f = std::bind(&Server::on_zz_material_state, this, std::placeholders::_1);
    zz_ctx_->set_material_state(f);
    zz_ctx_->set_next_task(std::bind(&Server::on_zz_next_task, this,
                    std::placeholders::_1, std::placeholders::_2));
}


} // lynx

