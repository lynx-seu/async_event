
#include "eventloop.h"
#include "config.h"
#include "server.h"

int main()
{
    CONFIG_INS->init("config.json");
    auto server_info = CONFIG_INS->get_server_info();
    auto core_info = CONFIG_INS->get_core_info();

    lynx::EventLoop loop;
    
    lynx::Server server(&loop, server_info.ip.c_str(), server_info.port);
    server.connect(core_info.ip.c_str(), core_info.port);

    loop.start();
    return 0;
}

