
#pragma once 

#include <string>
#include <memory>
#include <list>
#include <functional>

namespace lynx {
namespace dcs {

enum AgvPosition {
    AGV_LEFT = 0,
    AGV_RIGHT,
    MAX_POSITION,
};
    
enum TaskType {
    DELIVERY    = 0,        // 送
    PICK_UP     = 1,        // 取
};

enum MaterialType {
    AL_BOX            = 0,      // 铝盒
    COVER_BOARD       = 1,      // 盖板
    EMPTY_AL_BOX      = 2,      // 空铝盒
    EMPTY_COVER_BOARD = 3,      // 空盖板
    MAX_MATERIAL,
    MATERIAL_NONE,
};

enum TaskState {
    DCS_ZZ_NONE         = 0,        // DCS向AGV传递0
    DCS_ZZ_PROCESS      = 1,        // ...
    DCS_ZZ_LOCATE_DONE  = 2,
    DCS_ZZ_DONE         = 3,
    AGV_ZZ_NONE         = 0,        // AGV向DCS传递0
    AGV_ZZ_ALLOW        = 1,        // ...
    AGV_ZZ_DONE         = 2,
    
    ALL_DONE            = 1<<8,
};

// hard code
int get_material_order(MaterialType type);
std::string get_material_desc(MaterialType type);
std::string get_line_by_pos(const std::string& pos);

// hard code
static std::string get_material_pos(MaterialType type) {
    static const std::string material_pos[] = {
        "3",
        "1",
        "4",
        "2",
    };
    return material_pos[type];
}

struct TaskNode {
    void set_state(TaskState st) {state = st;}
    
    bool operator==(const TaskNode& n) const;
    bool operator!=(const TaskNode& n) const;

    // serialize
    std::string dump() const; 

    // deserialize
    static TaskNode parse(const std::string& in, std::string& err);
    static TaskNode parse(const char *in, std::string& err) {
        std::string in_str = in ? std::string(in) : "";
        return TaskNode::parse(in_str, err);
    }

    // properties
    std::string id;
    std::string start_pos, end_pos;
    std::string batch_number;
    int         seq;
    TaskType    type;
    TaskState   state;
    MaterialType    material; 
};

class MsgValue;

class Msg final
{
public:
    enum Type {
        NUL, TASK, TASK_COMPLETED, QUERY, BATCH
    };

    Msg();
    // 长度区分是否为 TASK_COMPLETED/QUERY/BATCH
    explicit Msg(const std::string& value);
    // TASK
    explicit Msg(const TaskNode& value);

    Type type() const;
    bool is_null() const {return type() == NUL;}
    bool is_task() const {return type() == TASK;}
    bool is_task_completed() const {return type() == TASK_COMPLETED;}
    bool is_query() const {return type() == QUERY;}
    bool is_batch() const {return type() == BATCH;}
    const std::string& string_value() const;
    const TaskNode& tasknode_value() const;
    const std::string& task_id() const;

    std::string dump() const;
    // 根据报文长度区分报文类型
    static Msg parse(const char *in, size_t len, std::string& err);
    static Msg parse(const std::string& in, std::string& err) {
        return Msg::parse(in.c_str(), in.size(), err);
    }

private:
    std::shared_ptr<MsgValue> value_;
};

class MsgValue 
{
protected:
    friend class Msg;
    virtual ~MsgValue() {}
    virtual Msg::Type type() const = 0;
    virtual void dump(std::string& out) const = 0;
    virtual const std::string& string_value() const;
    virtual const std::string& task_id() const;
    virtual const TaskNode& tasknode_value() const;
};

class ZZTaskMgr
{
public:
    // 计算agv到某点的距离
    // int 表示agv的id, std::string 表示某点
    using distance_func = std::function<float (int, const std::string&)>;
    
    ZZTaskMgr();
    ~ZZTaskMgr();
    ZZTaskMgr(const ZZTaskMgr&) = delete;
    ZZTaskMgr& operator=(const ZZTaskMgr&) = delete;

    // 添加dcs传入的任务
    void add_task(const TaskNode& task);
    bool find_a_task(TaskNode& task) const;
    bool contain_task(const TaskNode& task) const;
    void finish_task(const std::string& id);

    // 查找与task 匹配的AGV
    int find_agv(const TaskNode& task, const distance_func& distance) const;
    
    // 更新物料信息
    void update_material(const std::string&);
    bool exist_material(MaterialType type) const;
    
    // debug info
    std::string dump() const;

    // agv 与 任务id 关联
    void assign(int agv, const std::string& id);
    // agv 与 运送什么材料 关联
    void assign(int agv, AgvPosition position, MaterialType type);
    // agv载物 与 物料批号 关联
    void assign(int agv, AgvPosition position, const std::string& batch);
    
    std::string get_task_id(int agv) const;
    MaterialType get_material(int agv, AgvPosition position);
    std::string get_batch(int agv, AgvPosition position);
private:
    bool task_is_processing(const std::string& task) const;

private:
    struct Impl;
    std::unique_ptr<Impl> pimpl_;
};


} // dcs
} // lynx
