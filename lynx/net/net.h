
#pragma once

#include <string>
#include <exception>
#include "try.h"

namespace lynx {
namespace net {

class NetError : public std::exception
{
public:
    NetError(int ecode, const std::string& s)
        : ecode_(ecode), error_string_(s) {}

    const char* what() const noexcept override {
        return error_string_.data();
    }

    int ecode() const {
        return ecode_;
    }

private:
    int ecode_;
    std::string error_string_;
};

struct Session {
    int fd;
    int port;
    std::string ip;
};

// Try  -- maybe a result or exception
typedef int sock_t;
Try<sock_t> server(const char *ip, int port);
Try<Session> accept(sock_t server_fd);
Try<sock_t> connect(const char *ip, int port);
Try<sock_t> connect_nonblock(const char *ip, int port);

void close(sock_t fd);
    
int sock_pipe(int fds[2]);

// wrapper of system write/read
int write(sock_t fd, const char *buf, int len);
int read(sock_t fd, char *buf, int len);

// maybe throw a exception
void unsafe_set_nonblock(sock_t fd);
void unsafe_set_keepalive(sock_t fd);
void unsafe_set_nodelay(sock_t fd);


} // net
} // lynx
