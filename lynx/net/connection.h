#pragma once

#include <string>
#include <functional>
#include "buffer.h"
#include "detail/hacky_unique_function.h"


namespace lynx {

class EventLoop;
class Connection;

class Connection
{
    using FUNC = detail::hacky_unique_function<void (Connection*)>;
    using MessageFunc = detail::hacky_unique_function<size_t (Connection*, const char *, size_t)>;
public:
    explicit Connection(EventLoop *el, int fd);
    ~Connection();

    Connection(const Connection&) = delete;
    void operator=(const Connection&) = delete;

    EventLoop *get_loop() const {return loop_;}
    void shutdown();
    void write(const char* buf, size_t len);
    void write(const std::string& content) {
        write(content.c_str(), content.size());
    }

    template<class F>
    void set_on_close(F&& cb) { on_close_ = std::forward<F>(cb); }
    template<class F>
    void set_on_message(F&& cb) { on_message_ = std::forward<F>(cb); }

    bool valid() const { return state_ == CONNECTED; }
private:
    void handle_read();
    void handle_write();
    void handle_close();

private:
    enum State {
        NONE,
        CONNECTED,
        CLOSING,
        CLOSED,
    };

    EventLoop *const loop_;
    int    fd_;
    State  state_;
    Buffer read_buf_;
    Buffer write_buf_;

    // callback
    FUNC        on_close_;
    MessageFunc on_message_;
};

} // lynx
