
#include "buffer.h"
#include <cassert>
#include <algorithm>

namespace lynx {

static const size_t DEFAULT_SIZE = 64;

Buffer::Buffer()
    : buf_(DEFAULT_SIZE)
    , read_pos_ (0)
    , write_pos_(0)
{
}

Buffer::Buffer(const char* data, size_t size)
    : buf_(DEFAULT_SIZE)
    , read_pos_ (0)
    , write_pos_(0)
{
    write(data, size);
}

Buffer::~Buffer() {}

void Buffer::assure_space(size_t len)
{
    if (writable_size() >= len) return;

    if (writable_size() + read_pos_ < len) {
        buf_.resize(write_pos_ + len);
    }
    else {
        size_t buf_len = readable_size();
        std::copy(begin()+read_pos_, begin()+write_pos_, begin());
        read_pos_ = 0;
        write_pos_ = buf_len;
    }
}

void Buffer::write(const char* data, size_t len)
{
    if (!data || len == 0) return;
    assure_space(len);
    assert(len <= writable_size());
    std::copy(data, data+len, begin()+write_pos_);
    write_pos_ += len;
}

Slice Buffer::peek() const
{
    return Slice(begin()+read_pos_, readable_size());
}

void Buffer::consume(size_t len)
{
    if (len >= readable_size()) 
        clear();
    else 
        read_pos_ += len;
}


} // lynx

