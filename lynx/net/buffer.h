#pragma once

#include <vector>

namespace lynx {

using size_t = std::size_t;

struct Slice {
    const char* data;
    size_t len;

    explicit
    Slice(const char* d = nullptr, size_t l = 0): data(d), len(l) {}
};

class Buffer final
{
public:
    Buffer();
    Buffer(const char* data, size_t size);
    ~Buffer();

    Buffer(const Buffer&) = delete;
    void operator=(const Buffer&) = delete;
    Buffer(Buffer&&) = default; 
    Buffer& operator=(Buffer&&) = default;

    bool empty() const;
    size_t readable_size() const;
    size_t writable_size() const;
    void assure_space(size_t len);
    void write(const char* data, size_t len);
    Slice peek() const;
    void consume(size_t len);
    void clear();
private:
    char *begin() {return &*buf_.begin();}
    const char *begin() const {return &*buf_.begin();}
    
private:
    std::vector<char> buf_;
    size_t read_pos_;
    size_t write_pos_;
};


inline bool Buffer::empty() const
{
    return readable_size() == 0;
}

inline size_t Buffer::readable_size() const
{
    return write_pos_ - read_pos_;
}

inline size_t Buffer::writable_size() const
{
    return buf_.size() - write_pos_;
}

inline void Buffer::clear()
{
    read_pos_ = write_pos_ = 0;
}


} // lynx
