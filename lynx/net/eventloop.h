
#pragma once

#include <functional>
#include <memory>
#include <climits>
#include <type_traits>
#include "detail/helper.h"
#include "detail/hacky_unique_function.h"

namespace lynx {
 
// simulate infinity value
static const size_t MATH_HUGE = UINT_MAX;

// async event loop && timer
class EventLoop final {
public:
    using IoFn      = detail::hacky_unique_function<void ()>;
    using TimerFn   = detail::hacky_unique_function<void (long long)>;

    EventLoop();
    ~EventLoop();

    void start();
    void stop();
    void process_evts();

    template<class F, class... Args>
    inline void async_read(int fd, F&& f, Args&&... args) {
        static_assert(detail::is_callable<F, int, Args...>::value,
                    "F type must be F(int fd, Args...)");
        auto io_fn = [f=std::forward<F>(f), fd, args...]() mutable {
            std::forward<F>(f)(fd, std::forward<Args>(args)...);
        };
        IoFn&& fn = std::forward<decltype(io_fn)>(io_fn);
        async_read_impl(fd, std::move(fn));
    }

    template<class F, class... Args>
    inline void async_write(int fd, F&& f, Args&&... args) {
        static_assert(detail::is_callable<F, int, Args...>::value,
                    "F type must be F(int fd, Args...)");
        auto io_fn = [f=std::forward<F>(f), fd, args...]() mutable {
            std::forward<F>(f)(fd, std::forward<Args>(args)...);
        };
        IoFn&& fn = std::forward<decltype(io_fn)>(io_fn);
        async_write_impl(fd, std::move(fn));
    }

    template<class F, class... Args>
    inline long long every(long long ms, size_t times, F&& f, Args&&... args) {
        static_assert(detail::is_callable<F, long long, Args...>::value,
                    "F type must be F(long long timer_id, Args...)");
        auto io_fn = [f=std::forward<F>(f), args...](long long id) mutable {
            std::forward<F>(f)(id, std::forward<Args>(args)...);
        };
        TimerFn&& fn = std::forward<decltype(io_fn)>(io_fn);
        return every_impl(ms, times, std::move(fn));
    }

    template<class F, class... Args>
    inline typename std::enable_if<
            detail::is_callable<F, long long, Args...>::value,
            long long>::type
    every(long long ms, F&& f, Args&&... args) {
        return every(ms, MATH_HUGE, std::forward<F>(f), std::forward<Args>(args)...);
    }

    template<class F, class... Args>
    inline long long after(long long ms, F&& f, Args&&... args) {
        auto io_fn = [f=std::forward<F>(f), args...](long long) mutable {
            std::forward<F>(f)(std::forward<Args>(args)...);
        };
        TimerFn&& fn = std::forward<decltype(io_fn)>(io_fn);
        return every_impl(ms, 1, std::move(fn));
    }

    void del_async_read_fn(int fd);
    void del_async_write_fn(int fd);
    void del_timer_id(long long id);
    
    template<class F, class... Args>
    void spawn(F&& f, Args&&... args) {
        auto fn = [f=std::forward<F>(f), args...]() mutable {
            std::forward<F>(f)(std::forward<Args>(args)...);
        };
        spawn_impl(std::move(fn));
    }
private:
    void spawn_impl(IoFn&& fn);
    void async_read_impl(int fd,  IoFn&& fn);
    void async_write_impl(int fd, IoFn&& fn);
    long long every_impl(long long ms, size_t times, TimerFn&& fn);

private:
    // follow: [pimpl idiom](https://docs.microsoft.com/en-us/cpp/cpp/pimpl-for-compile-time-encapsulation-modern-cpp)
    struct Impl;
    std::unique_ptr<Impl> impl_;
};
} // end namespace lynx

