
#include "connection.h"
#include "eventloop.h"
#include "net.h"

namespace lynx {

Connection::Connection(EventLoop *el, int fd)
    : loop_(el)
    , fd_(fd)
    , state_(CONNECTED)
{
    lynx::net::unsafe_set_nonblock(fd);
    loop_->async_read(fd, [this](int) {handle_read();});
}

Connection::~Connection()
{
    shutdown();
}

void Connection::shutdown()
{
    if (state_ != CLOSED) {
        state_ = CLOSED;
        net::close(fd_);
        loop_->del_async_read_fn(fd_);
        loop_->del_async_write_fn(fd_);
    }
}

void Connection::write(const char* buf, size_t len)
{
    // 未连接先放入缓冲区
    if (state_ == NONE) {
        write_buf_.write(buf, len);
    }
    else if (state_ == CONNECTED)  {
        if (write_buf_.empty()) {
            loop_->async_write(fd_, [this] (int) {
                handle_write();
            });
        }
        write_buf_.write(buf, len);
    }
}

// readv
void Connection::handle_read()
{
    if (state_ != CONNECTED) return;

    char buf[4 * 1024];
    int len;

    do {
        len = lynx::net::read(fd_, buf, sizeof(buf));
        if (len > 0)
            read_buf_.write(buf, len);
        else if (len < 0) {
            state_ = CLOSING;
            handle_close();
        }
    } while (len == sizeof(buf));

    while (read_buf_.readable_size() > 0) {
        auto slice = read_buf_.peek();
        size_t bytes = 0;
        if (on_message_)
            bytes = on_message_(this, slice.data, slice.len);

        if (bytes == 0) {
            break;
        } else {
            read_buf_.consume(bytes);
        }
    }
    
    // on_close must be at the bottom
    if (state_ == CLOSED && on_close_) on_close_(this);
}

// writev
void Connection::handle_write()
{
    if (state_ != CONNECTED) return;

    // 写入
    if (write_buf_.readable_size() > 0) {
        auto slice = write_buf_.peek();
        int n = lynx::net::write(fd_, slice.data, slice.len);
        write_buf_.consume(n);
    }

    // 写入缓冲区为空, 不再监测写事件
    if (write_buf_.readable_size() == 0) {
        loop_->del_async_write_fn(fd_);
    }
}

void Connection::handle_close()
{
    shutdown();
}

} // lynx

