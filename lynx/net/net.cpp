
#include "net.h"

#ifdef WIN32
#   include <winsock2.h>
#   include <ws2tcpip.h>
#else
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <netinet/tcp.h>
#   include <arpa/inet.h>
#   include <unistd.h>
#   include <fcntl.h>
#   include <netdb.h>

#   define INVALID_SOCKET       -1
#   define SOCKET_ERROR         -1
#endif

#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef WIN32
void close(int sock)
{
    closesocket(sock);
}
#endif

namespace lynx {
namespace net {

static const int CONNECT_NONBLOCK = 1;

inline void throw_error(const char *prefix, int err)
{
    if (err == -1) {
        throw NetError(err, std::string(prefix));
    }
    else {
        throw NetError(err, std::string(prefix) + ": " + strerror(err));
    }
}

static int unsafe_tcp_server(const char *ip, int port)
{    
    int s, on = 1;
    struct sockaddr_in sa;
    
    if ((s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == (sock_t)INVALID_SOCKET) {
        throw_error("socket", errno);
    }
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const char *)&on, sizeof(on)) == -1) {
        ::close(s);
        throw_error("setsockopt SO_REUSEADDR", errno);
    }
    memset(&sa,0,sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (ip) {
#ifdef WIN32
        sa.sin_addr.s_addr = inet_addr(ip);
#else
        if (inet_aton(ip, &sa.sin_addr) == 0) {
            ::close(s);
            throw_error("Invalid bind address", -1);
        }
#endif
    }
    if (bind(s, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR) {
        ::close(s);
        throw_error("bind", errno);
    }
    if (listen(s, 64) == SOCKET_ERROR) {
        ::close(s);
        throw_error("listen", errno);
    }

    return s;
}

Try<sock_t> server(const char *ip, int port)
{
    return wrap_with_try(unsafe_tcp_server, ip, port);
}

Try<Session> accept(sock_t server)
{
    auto unsafe_accept = [server]() mutable {
        int fd;
        struct sockaddr_in sa;
#ifdef WIN32
        int sa_len;
#else
        unsigned int sa_len;
#endif

        while(1) {
            sa_len = sizeof(sa);
            fd = ::accept(server, (struct sockaddr*)&sa, &sa_len);
            if (fd == -1) {
                if (errno == EINTR)
                    continue;
                else {
                    throw_error("accept", errno);
                }
            }
            break;
        }
        return Session {
            .fd     = fd,
            .port   = ntohs(sa.sin_port),
            .ip     = inet_ntoa(sa.sin_addr),
        };
    };

    return wrap_with_try(unsafe_accept);
}

static sock_t unsafe_connect(const char *ip, int port, int flags)
{
    int s, on = 1;
    struct sockaddr_in sa;

    /**
     * 创建socket套接字
     */
    if ((s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == (sock_t)INVALID_SOCKET) {
        throw_error("creating socket", errno);
    }
    /* Make sure connection-intensive things like the redis benckmark
     * will be able to close/open sockets a zillion of times */
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const char *)&on, sizeof(on));

    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);

#ifdef WIN32
    sa.sin_addr.s_addr = inet_addr(ip);
#else
    /**
     * inet_aton是一个改进的方法，用来将一个字符串IP地址转换为一个
     * 32位的网络序列地址
     */
    if (inet_aton(ip, &sa.sin_addr) == 0) {
        struct hostent *he;

        he = gethostbyname(ip);
        if (he == NULL) {
            ::close(s);
            throw_error((std::string("can't resolve") + ip).c_str(), -1);
        }
        memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
    }
#endif

    if (flags & CONNECT_NONBLOCK) {
        unsafe_set_nonblock(s);
    }

    if (::connect(s, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR) {
        if (errno == EINPROGRESS &&
            flags & CONNECT_NONBLOCK)
            return s;
        
#ifdef WIN32
        // if socket noblock, winsock throw error WSAEWOULDBLOCK
        if (WSAGetLastError() == WSAEWOULDBLOCK && flags & CONNECT_NONBLOCK)
            return s;
#endif

        ::close(s);
        throw_error("connect", errno);
    }
    return s;
}

Try<sock_t> connect(const char *ip, int port)
{
    return wrap_with_try(unsafe_connect, ip, port, ~CONNECT_NONBLOCK);
}

// [ref](https://www.scottklement.com/rpg/socktut/nonblocking.html)
// 非阻塞socket, connect会返回'Operation In Progress', 再次调用conenct会返回
//  'Operation Already In Progress' 或者 返回 a successful return code
Try<sock_t> connect_nonblock(const char *ip, int port)
{
    return wrap_with_try(unsafe_connect, ip, port, CONNECT_NONBLOCK);
}

void close(sock_t fd)
{
    if(fd != INVALID_SOCKET) ::close(fd);
}

void unsafe_set_nonblock(sock_t fd)
{
#ifdef WIN32
    /// @note windows sockets are created in blocking mode by default
    // currently on windows, there is no easy way to obtain the socket's current blocking mode since WSAIsBlocking was deprecated
    u_long flags = 1;
    if (NO_ERROR != ioctlsocket(fd, FIONBIO, &flags)) {
        throw_error("ioctlsocket(FIONBIO)", errno);
    }
#else
    int flags;
    /* Set the socket nonblocking.
    * Note that fcntl(2) for F_GETFL and F_SETFL can't be
    * interrupted by a signal. */
    // fcntl函数可以改变已打开的文件性质
    // 获取文件描述符fd上的标志位
    if ((flags = fcntl(fd, F_GETFL)) == -1) {
        throw_error("fcntl(F_GETFL)", errno);
    }
    //给当前的文件描述符设置非阻塞标志位
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
        throw_error("fcntl(F_SETFL,O_NONBLOCK)", errno);
    }
#endif
}
    
int sock_pipe(int fds[2])
{
#ifdef WIN32
    struct sockaddr_in addr = { 0 };
    int addr_size = sizeof (addr);
    struct sockaddr_in adr2;
    int adr2_size = sizeof (adr2);
    int listener;
    int sock [2] = { -1, -1 };
    
    if ((listener = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
        return -1;
    
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    addr.sin_port = 0;
    
    if (bind (listener, (struct sockaddr *)&addr, addr_size))
        goto fail;
    
    if (getsockname(listener, (struct sockaddr *)&addr, &addr_size))
        goto fail;
    
    if (listen(listener, 1))
        goto fail;
    
    if ((sock[0] = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
        goto fail;
    
    if (::connect(sock[0], (struct sockaddr *)&addr, addr_size))
        goto fail;
    
    /* TODO: returns INVALID_SOCKET on winsock accept, not < 0. fix it */
    /* when convenient, probably by just removing error checking altogether? */
    if ((sock [1] = ::accept(listener, 0, 0)) < 0)
        goto fail;
    
    /* windows vista returns fantasy port numbers for sockets:
     * example for two interconnected tcp sockets:
     *
     * (Socket::unpack_sockaddr_in getsockname $sock0)[0] == 53364
     * (Socket::unpack_sockaddr_in getpeername $sock0)[0] == 53363
     * (Socket::unpack_sockaddr_in getsockname $sock1)[0] == 53363
     * (Socket::unpack_sockaddr_in getpeername $sock1)[0] == 53365
     *
     * wow! tridirectional sockets!
     *
     * this way of checking ports seems to work:
     */
    if (getpeername(sock [0], (struct sockaddr *)&addr, &addr_size))
        goto fail;
    
    if (getsockname(sock [1], (struct sockaddr *)&adr2, &adr2_size))
        goto fail;
    
    errno = WSAEINVAL;
    if (addr_size != adr2_size
        || addr.sin_addr.s_addr != adr2.sin_addr.s_addr /* just to be sure, I mean, it's windows */
        || addr.sin_port        != adr2.sin_port)
        goto fail;
    
    closesocket (listener);
    fds[0] = sock[0];
    fds[1] = sock[1];

    return 0;
fail:
    closesocket (listener);
    if (sock[0] != INVALID_SOCKET) closesocket(sock[0]);
    if (sock[1] != INVALID_SOCKET) closesocket(sock[1]);
    
    return -1;
#else
    return ::pipe(fds);
#endif
}
    

void unsafe_set_keepalive(sock_t fd)
{
    int yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (const char *)&yes, sizeof(yes)) == -1) 
        throw_error("setsockopt SO_KEEPALIVE", errno);
}

void unsafe_set_nodelay(sock_t fd)
{   
    int yes = 1;
    if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (const char *)&yes, sizeof(yes)) == -1) 
        throw_error("setsockopt TCP_NODELAY", errno);
}

int write(sock_t fd, const char *buf, int len)
{
    int nwritten, totlen = 0;
    while(totlen != len) {
#ifdef WIN32
        nwritten = ::send(fd,buf,len-totlen,0);
#else
        nwritten = ::write(fd,buf,len-totlen);
#endif
        if (nwritten == 0) return totlen;
        if (nwritten == -1) return -1;
        totlen += nwritten;
        buf += nwritten;
    }
    return totlen;
}

int read(sock_t fd, char *buf, int len)
{
#ifdef WIN32
    int nread = ::recv(fd, buf, len, 0);
#else
    int nread = ::read(fd, buf, len);
#endif
    if (nread == -1 && errno == EAGAIN) {
        return 0;
    }

    // n == 0   client close connection
    // n == -1  read errors
    return nread == 0 ? -1 : nread;
}

} // net
} // lynx
