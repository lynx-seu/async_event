
#include <chrono>
#include <vector>
#include <map>
#include <algorithm>
#include <cstring>
#include <cassert>
#include <mutex>
#include <deque>

#include "eventloop.h"
#include "net.h"
#ifdef WIN32
#   include <winsock2.h>
#else
#   include <sys/select.h>
#endif

namespace lynx {
using std::chrono::system_clock;
using std::chrono::duration;
using std::chrono::duration_cast;

class Poller
{
public:
    enum {
        in  = 1,
        out = 2,
    };

    virtual ~Poller() {}
    virtual bool resize(size_t size)   = 0;
    virtual bool add_event(int fd, int mask) = 0;
    virtual void del_event(int fd, int mask) = 0;
    virtual void poll(struct timeval *) = 0;
};

struct TimerHandle {
    size_t                   counts;
    long long                interval;
    system_clock::time_point when;
    EventLoop::TimerFn       proc;
};

struct EventLoop::Impl {
    int                     maxfd = -1;
    long long               next_timer_id = 0;
    bool                    stop = false;
    std::shared_ptr<Poller> poller = nullptr;
    std::map<int, IoFn>     read_fns;
    std::map<int, IoFn>     write_fns;
    std::map<long long, TimerHandle> timer_fns;
    
    // tasks;
    int              pipe[2];
    std::mutex       mtx;
    std::deque<IoFn> tasks;
    
    // helper for deleted event callback (fd)
    std::vector<int> to_remove_read_fd;
    std::vector<int> to_remove_write_fd;

    void add_async_event(int fd, int mask, IoFn&& callback) {
        // add to poller
        if (poller && poller->add_event(fd, mask)) {
            // record
            if (mask == Poller::in) {
                auto iter = std::find(to_remove_read_fd.begin(),
                                    to_remove_read_fd.end(), fd);
                assert(read_fns.find(fd) == read_fns.end() ||
                    iter != to_remove_read_fd.end());
                read_fns[fd] = std::move(callback);
            }
            else if (mask == Poller::out) {
                auto iter = std::find(to_remove_write_fd.begin(),
                                    to_remove_write_fd.end(), fd);
                assert(write_fns.find(fd) == write_fns.end() ||
                                    iter != to_remove_write_fd.end());
                write_fns[fd] = std::move(callback);
            }

            // maxfd
            if (maxfd < fd) maxfd = fd;

            // 
            all_fds.push_back(fd);
        }
    }

    void del_async_event(int fd, int mask) {
        if (!poller) return;
        poller->del_event(fd, mask);

        auto remove_fd = [](std::map<int, IoFn>& m, int fd) {
            auto iter = m.find(fd);
            if (iter != m.end()) {
                m.erase(iter);
            }
        };

        // remove fd
        if (mask == Poller::in) {
            remove_fd(read_fns, fd);
        }
        else if (mask == Poller::out) {
            remove_fd(write_fns, fd);
        }

        auto iter = std::find(all_fds.begin(), all_fds.end(), fd);
        if (iter != all_fds.end()) all_fds.erase(iter);

        // maxfd
        if (maxfd == fd) {
            if (all_fds.size() == 0) {
                maxfd = -1;
            }
            else {
                std::sort(all_fds.begin(), all_fds.end());
                maxfd = all_fds.back();
            }
        }
    }


private:
    // helper for maxfd (binary_heap)
    std::vector<int> all_fds;
};

class SelectPoller : public Poller
{
public:
    SelectPoller(const int& maxfd, const std::map<int, EventLoop::IoFn>& read_fns,
                const std::map<int, EventLoop::IoFn>& write_fns)
        : maxfd_(maxfd), read_fns_(read_fns), write_fns_(write_fns)
    {
        FD_ZERO(&rfds_);
        FD_ZERO(&wfds_);
    }

    ~SelectPoller() {
    }

    bool resize(size_t size) override {
        return size < FD_SETSIZE;
    }

    bool add_event(int fd, int mode) override {
        if (mode & Poller::in)  FD_SET(fd, &rfds_);
        if (mode & Poller::out) FD_SET(fd, &wfds_);
        return true;
    }

    void del_event(int fd, int mode) override {
        if (mode & Poller::in)  FD_CLR(fd, &rfds_);
        if (mode & Poller::out) FD_CLR(fd, &wfds_);
    }

    void poll(struct timeval *tvp) override {
        fd_set rfds, wfds;
        memcpy(&rfds, &rfds_, sizeof(fd_set));
        memcpy(&wfds, &wfds_, sizeof(fd_set));

        int retval = select(maxfd_+1, &rfds, &wfds, nullptr, tvp);
        if (retval > 0) {
            // fixed: 异步io函数执行过程中，可能会从poller删除异步io fd
            // find all read events
            for (const auto &kv : read_fns_) {
                if (FD_ISSET(kv.first, &rfds)) kv.second();
            }

            // find all write events
            for (const auto &kv : write_fns_) {
                if (FD_ISSET(kv.first, &wfds)) kv.second();
            }
        }
    }
private:
    fd_set rfds_, wfds_;
    const int&                             maxfd_;
    const std::map<int, EventLoop::IoFn>&  read_fns_;
    const std::map<int, EventLoop::IoFn>&  write_fns_;
};


/* * * * * * * * * * * * * * * * * * * *
 * EventLoop 
 */
EventLoop::EventLoop() : impl_(new Impl)
{
#if WIN32
	// Fixme: startup failed
	WORD wVersionRequested;
	WSADATA wsaData;

	wVersionRequested = MAKEWORD(2, 2);
	WSAStartup(wVersionRequested, &wsaData);
#endif

    if (impl_->poller == nullptr) {
        auto select_poller = new SelectPoller(impl_->maxfd, 
                                            impl_->read_fns, 
                                            impl_->write_fns);
        impl_->poller = std::shared_ptr<Poller>(select_poller);
    }
//    every(1000, [](long){});
    while (net::sock_pipe(impl_->pipe)) ;
    net::unsafe_set_nonblock(impl_->pipe[0]);
    net::unsafe_set_nonblock(impl_->pipe[1]);
    async_read(impl_->pipe[0], [](int fd) {
        char dummy[4];
        net::read(fd, dummy, 4);
    });
}

EventLoop::~EventLoop() 
{
#if WIN32
	WSACleanup();
#endif
    net::close(impl_->pipe[0]);
    net::close(impl_->pipe[1]);
}

void EventLoop::start() { while(!impl_->stop) process_evts(); }
void EventLoop::stop()  { impl_->stop = true; }

void EventLoop::process_evts()
{
    // task queue
    if (impl_->tasks.size() > 0) {
        std::lock_guard<std::mutex> lck(impl_->mtx);
        for (auto&& f: impl_->tasks) f();
        impl_->tasks.clear();
    }

    auto &time_evts = impl_->timer_fns;

    // sleep until events coming 
    struct timeval tv, *tvp = nullptr;
    auto shortest = time_evts.begin();
    decltype(shortest) iter;
    for (iter=time_evts.begin(); iter!=time_evts.end(); ++iter) {
            if (iter->second.when < shortest->second.when) shortest = iter;
    }

    if (shortest != time_evts.end()) {
        //shortest->when
        auto now = system_clock::now();
        auto ms = duration_cast<std::chrono::milliseconds>(
                     shortest->second.when - now
                );
        auto ms_count = ms.count();
        if (ms_count < 0) ms_count = 0;
        tv.tv_sec  = static_cast<long>(ms_count/1000);
        tv.tv_usec = static_cast<long>((ms_count%1000)*1000);
        tvp = &tv;
    }

    // poll io event
    impl_->poller->poll(tvp);
    // remove fd
    for (auto fd : impl_->to_remove_write_fd) {
        impl_->del_async_event(fd, Poller::out);
    }
    for (auto fd : impl_->to_remove_read_fd) {
        impl_->del_async_event(fd, Poller::in);
    }
    impl_->to_remove_write_fd.clear();
    impl_->to_remove_read_fd.clear();

    // process timer events
    auto now = system_clock::now();
    std::vector<long long> to_remove;
    for (auto &te: time_evts) {
        auto &th = te.second;
        if (th.when < now) {
            th.when += duration<int, std::milli>(th.interval);
            th.proc(te.first);
            if (th.counts != lynx::MATH_HUGE) th.counts--;
            if (th.counts == 0) to_remove.push_back(te.first);
        }
    }
    for (auto id: to_remove) del_timer_id(id);
}

void EventLoop::spawn_impl(IoFn&& fn)
{
    std::lock_guard<std::mutex> lock(impl_->mtx);
    impl_->tasks.push_back(std::move(fn));
    char buf[1] = {1};
    net::write(impl_->pipe[1], buf, 1);
}

void EventLoop::async_read_impl(int fd, IoFn&& fn) 
{
    impl_->add_async_event(fd, Poller::in, std::move(fn));
}

void EventLoop::async_write_impl(int fd, IoFn&& fn)
{
    impl_->add_async_event(fd, Poller::out, std::move(fn));
}

long long EventLoop::every_impl(long long ms, size_t times, TimerFn&& fn)
{
    auto id = impl_->next_timer_id++; 
    auto w  = system_clock::now() + duration<int, std::milli>(ms);
    TimerHandle th { times, ms, w, std::move(fn) };

    impl_->timer_fns.emplace(id, std::move(th));
    return id;
}

void EventLoop::del_async_read_fn(int fd) 
{
//    impl_->del_async_event(fd, Poller::in);
    impl_->to_remove_read_fd.push_back(fd);
}

void EventLoop::del_async_write_fn(int fd) 
{
//    impl_->del_async_event(fd, Poller::out);
    impl_->to_remove_write_fd.push_back(fd);
}

void EventLoop::del_timer_id(long long id) 
{ 
    auto &time_evts = impl_->timer_fns;
    auto iter = time_evts.find(id);
    if (iter != time_evts.end()) time_evts.erase(iter);
}

} // end namespace lynx

