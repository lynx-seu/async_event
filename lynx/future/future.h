// TODO: add all/any
#pragma once

#include <type_traits>
#include <string>
#include <mutex>
#include <condition_variable>
#include <exception>
#include <stdexcept>
#include <system_error>
#include <utility>
#include <memory>
#include "try.h"
#include "detail/hacky_unique_function.h"
#include "detail/future_helper.h"


namespace lynx {

enum class future_status {
    ready,
    timeout,
};

enum class timeout_state {
    not_set,
    expired,
    result_set,
};

enum errc {
    broken_promise = 0,
    future_already_retrieved,
    future_timeout,
    promise_already_satisfied,
    no_state,
    errc_max,
};

inline std::string errc_string(errc value) {
    static std::string errc_str[static_cast<size_t>(errc::errc_max)] = {
        "broken promise",
        "future already retrieved",
        "future timeout",
        "promise already satisfied",
        "no state",
    };
    return errc_str[value];
}

// future error
struct FutureError : public std::exception {
    FutureError(errc ecode, const std::string &s)
        : ecode_(ecode),
          error_string_(s) {}

    const char *what() const noexcept override { return error_string_.data(); }

    errc ecode() const { return ecode_; }

    errc ecode_;
    std::string error_string_;
};

template<typename T>
class Future;

template<typename T>
class Promise;

// helper function
namespace detail {

template <class T>
class AsyncState : public std::enable_shared_from_this<AsyncState<T>> 
{
    using value_type = try_wrapper_t<T>;
    using callback_func = hacky_unique_function<void(value_type&&)>;
public:

    bool is_ready() const {
        std::unique_lock<std::mutex> lock(mtx_);
        return unsafe_is_ready();
    }

    void wait() const {
        std::unique_lock<std::mutex> lock(mtx_);
        cond_.wait(lock, [this] { return unsafe_is_ready(); });
    }

    template <typename Rep, typename Period>
    future_status wait_for(const std::chrono::duration<Rep, Period> &timeout) const {
        std::unique_lock<std::mutex> lock(mtx_);
        cond_.wait_for(lock, timeout, [this] { return unsafe_is_ready(); });
        return unsafe_is_ready() ? future_status::ready : future_status::timeout;
    }

    template <typename Clock, typename Duration>
    future_status wait_until(const std::chrono::time_point<Clock, Duration> &timepoint) const {
        std::unique_lock<std::mutex> lock(mtx_);
        cond_.wait_until(lock, timepoint, [this] { return unsafe_is_ready(); });
        return unsafe_is_ready() ? future_status::ready : future_status::timeout;
    }

    void set_value(value_type&& value) {
        std::unique_lock<std::mutex> lock(mtx_);
        throw_if_satisfied();
        value_ = std::move(value);
        satisfied_ = true;
        // future ready
        cond_.notify_all();
        if (f_) {
            f_(std::move(value_));
        }
    }

    value_type get_value() const {
        wait();
        return std::move(value_);
    }

    template<class F>
    void set_callback(F&& f) {
        std::unique_lock<std::mutex> lock(mtx_);
        f_ = std::forward<F>(f);
        if (unsafe_is_ready()) {
            // the state is ready, invoke the continuation immediately
            // unlock here so that the Future passed to the continuation below doesn't block in .get()
            f_(std::move(value_));
        }
    }

    void abort() {
        std::unique_lock<std::mutex> lock(mtx_);
        if (unsafe_is_ready()) return;

        auto exception_ptr = std::make_exception_ptr(
                        FutureError(errc::broken_promise,
                        errc_string(errc::broken_promise)));
        lock.unlock();
        set_value(value_type(exception_ptr));
    }

    // helper for timeout
    timeout_state unsafe_expired() const { return timeout_helper_.state; }
    void unsafe_expired(timeout_state state) { timeout_helper_.state = state; }
    std::mutex& get_timeout_mtx() { return timeout_helper_.mtx; }

private:
    inline bool unsafe_is_ready() const { return satisfied_; } 

    inline void throw_if_satisfied() {
        if (unsafe_is_ready())
            throw FutureError(errc::promise_already_satisfied,
                               errc_string(errc::promise_already_satisfied));
    }

private:
    mutable std::mutex              mtx_;
    mutable std::condition_variable cond_;
    value_type                      value_;
    callback_func                   f_;
    bool                            satisfied_ {false};

    //timeout helper
    struct {
        std::mutex      mtx;
        timeout_state   state = timeout_state::not_set;
    } timeout_helper_;
};

template<typename T>
using async_state_t = std::shared_ptr<AsyncState<T>>;

template<typename T>
void check_state(const async_state_t<T>& state) {
    if (!state)
        throw FutureError(errc::no_state, errc_string(errc::no_state));
}


// fulfill promise
    
template<typename R, typename Inner>
typename std::enable_if<std::is_same<R, Inner>::value, void>::type
fulfill_promise(Promise<R>&& p, Try<Inner>&& r) {
    p.set_value(std::move(r));
}
    
template<typename R, typename Inner>
typename std::enable_if<std::is_void<R>::value &&
                    std::is_same<Inner, Future<void>>::value,
                    void>::type
fulfill_promise(Promise<R>&& p, Try<Inner>&& r) {
    if (r.has_exception()) {
        p.set_exception(r.exception());
    }
    else {
        auto&& future_result = r.value();
        future_result.then([p = std::move(p)]() mutable {
            p.set_value();
        });
    }
}
    
template<typename R, typename Inner>
typename std::enable_if<!std::is_void<R>::value &&
                    std::is_same<Inner, Future<R>>::value,
        void>::type
fulfill_promise(Promise<R>&& p, Try<Inner>&& r) {
    if (r.has_exception()) {
        p.set_exception(r.exception());
    }
    else {
        auto&& future_result = r.value();
        future_result.then([p = std::move(p)](R _v) mutable {
            p.set_value(std::forward<R>(_v));
        });
    }
}

// F(Predecessor) -> T
template<typename T, typename F, typename Predecessor>
typename std::enable_if<!callable_result<F, Predecessor>::is_future, void>::type
fulfill_promise(Promise<T>&& p, F&& f, Try<Predecessor>&& v) {
    static_assert(!std::is_same<Predecessor, try_wrapper_t<Predecessor>>::value, 
                  "Try can't be F parameter");
    auto&& result = wrap_with_try(std::forward<F>(f), std::move(v));
    p.set_value(std::move(result));
}

// F(Arg) -> Future<void>
template<typename T, typename F, typename Predecessor>
typename std::enable_if<callable_result<F, Predecessor>::is_future &&
                        std::is_void<T>::value, void>::type
fulfill_promise(Promise<T>&& p, F&& f, Try<Predecessor>&& v) {
    static_assert(!std::is_same<Predecessor, try_wrapper_t<Predecessor>>::value, 
                  "Try can't be the parameter of F");
    auto&& maybe_future_result = wrap_with_try(std::forward<F>(f), std::move(v));

    if (maybe_future_result.has_exception()) {
        p.set_exception(maybe_future_result.exception());
    }
    else {
        auto&& future_result = maybe_future_result.value();
        future_result.then([p = std::move(p)]() mutable {
            p.set_value();
        });
    }
}
    
// F(Arg) -> Future<T>
template<typename T, typename F, typename Predecessor>
typename std::enable_if<callable_result<F, Predecessor>::is_future &&
                    !std::is_void<T>::value, void>::type
fulfill_promise(Promise<T>&& p, F&& f, Try<Predecessor>&& v) {
    static_assert(!std::is_same<Predecessor, try_wrapper_t<Predecessor>>::value,
                  "Try can't be the parameter of F");
    auto&& maybe_future_result = wrap_with_try(std::forward<F>(f), std::move(v));

    if (maybe_future_result.has_exception()) {
        p.set_exception(maybe_future_result.exception());
    }
    else {
        auto&& future_result = maybe_future_result.value();
        future_result.then([p = std::move(p)](T _v) mutable {
            p.set_value(std::forward<T>(_v));
        });
    }
}
    

} // detail


template<typename T>
class Future {
    using value_type = try_wrapper_t<T>;

    static_assert(!std::is_same<T, try_wrapper_t<T>>::value, "T must be not Try type");
    static_assert(
        std::is_void<T>::value || std::is_default_constructible<T>::value,
        "T must be default-constructible or void");

    template<typename U>
    friend class Promise;
public:
    Future() = default;

    Future(const Future<T> &other) = delete;
    Future<T> &operator=(const Future<T> &other) = delete;

    Future(Future<T> &&other)
        : state_(std::move(other.state_)) {}

    Future<T> &operator=(Future<T> &&other) {
        state_ = std::move(other.state_);
        return *this;
    }

    bool valid() const {
        return state_ != nullptr;
    }


    Try<T>&& get() {
        check_state(state_);
        auto&& v = state_->get_value();
        return std::move(v);
    }

    template <typename F>
    detail::then_ret_t<F, T>
    then(F &&f) {
        check_state(state_);
        return then_impl<F>(std::forward<F>(f));
    }
    
    template <typename Timer>
    Future<T> timeout(Timer &timer, long long ms) {
        auto promise_ptr = std::make_shared<Promise<T>>();
        Future<T> ret = promise_ptr->get_future();
        
        // 超时
        timer.after(ms, [promise_ptr, state = state_->shared_from_this()]() mutable {
            std::unique_lock<std::mutex> lck(state->get_timeout_mtx());
            if (state->unsafe_expired() == timeout_state::result_set) return;
            state->unsafe_expired(timeout_state::expired);
            auto exception = FutureError(errc::future_timeout, errc_string(errc::future_timeout));
            lck.unlock();
            promise_ptr->set_exception(std::make_exception_ptr(exception));
        });

        // 正常的回调 (fix: 如果已经超时, 这时Promise析构或者set_value进入死锁)
        set_callback([promise_ptr, state = state_->shared_from_this()](Try<T>&& v) mutable {
            std::unique_lock<std::mutex> lck(state->get_timeout_mtx());
            if (state->unsafe_expired() == timeout_state::expired) return;
            state->unsafe_expired(timeout_state::result_set);
            promise_ptr->set_value(std::move(v));
        });

        return ret;
    }

    template <typename F, typename Executor>
    detail::then_ret_t<F, T> then(Executor &exec, F &&f) {
        check_state(state_);
        return then_impl<F>(exec, std::forward<F>(f));
    }

    bool is_ready() const {
        check_state(state_);
        return state_->is_ready();
    }

    void wait() const {
        check_state(state_);
        state_->wait();
    }

    template <typename Rep, typename Period>
    lynx::future_status wait_for(const std::chrono::duration<Rep, Period> &timeout) {
        check_state(state_);
        return state_->wait_for(timeout);
    }

    template <typename Clock, typename Duration>
    lynx::future_status wait_until(const std::chrono::time_point<Clock, Duration> &timepoint) {
        check_state(state_);
        return state_->wait_until(timepoint);
    }

private:
    Future(const detail::async_state_t<T> &state)
        : state_(state) {}

    // R F(T) specialization
    template <typename F>
    auto then_impl(F &&f) -> detail::then_ret_t<F, T> {
        using R = detail::future_inner_t<detail::then_ret_t<F, T>>;
        Promise<R> p;
        Future<R> ret = p.get_future();

        set_callback([p = std::move(p), f = std::forward<F>(f)] (value_type&& v) mutable {
            auto&& result = wrap_with_try(f, std::move(v));
            detail::fulfill_promise(std::move(p), std::move(result));
        });

        return ret;
    }

    template <typename F, typename Executor>
	auto then_impl(Executor &exec, F &&f) -> detail::then_ret_t<F, T> {
        using R = detail::future_inner_t<detail::then_ret_t<F, T>>;
        Promise<R> p;
        Future<R> ret = p.get_future();

        // TODO: test
        set_callback([p = std::move(p), f = std::forward<F>(f), &exec](value_type&& v) mutable {
            exec.spawn([p = std::move(p), f = std::forward<F>(f), v = std::move(v)]() mutable {
                auto&& result = wrap_with_try(f, std::move(v));
                detail::fulfill_promise(std::move(p), std::move(result));
            });
        });

		return ret;
	}

    template <typename F>
    void set_callback(F &&f) {
        check_state(state_);
        state_->set_callback(std::forward<F>(f));
    }

private:
    detail::async_state_t<T> state_;
};

template<typename T>
Future<T> make_exceptional_future(std::exception_ptr p);

// TODO: shared_future
// TODO: T& specialization. Forbid and test.
template<typename T>
class Future<T&>;

template<typename T>
class Promise;

template <typename T>
class Promise {
public:
    Promise()
        : state_(std::make_shared<detail::AsyncState<T>>()) {}

    Promise(Promise &&other)
        : state_(std::move(other.state_)) {}

    Promise &operator=(Promise &&other) {
        state_ = std::move(other.state_);
        return *this;
    }

    ~Promise() {
        if (state_)
            state_->abort();
    }

    void swap(Promise &other) noexcept {
        state_.swap(other.state_);
    }

    template <typename U>
    typename std::enable_if<!std::is_same<U, try_wrapper_t<U>>::value, void>::type
    set_value(U&& value) {
        check_state(state_);
        auto&& x = try_wrapper_t<T>(std::forward<U>(value));
        state_->set_value(std::move(x));
    }

    template<typename U>
    void set_value(Try<U>&& value) {
        static_assert(std::is_same<T, U>::value, "T and U must be same type");
        check_state(state_);
        state_->set_value(std::forward<Try<U>>(value));
    }

    template<typename U = void>
    void set_value() {
        static_assert(std::is_void<T>::value, "T and U must be void type");
        check_state(state_);
        state_->set_value(try_wrapper_t<void>());
    }

    void set_exception(std::exception_ptr p) {
        check_state(state_);
        state_->set_value(try_wrapper_t<T>(p));
    }

    Future<T> get_future() {
        check_state(state_);
        if (state_.use_count() > 1) {
            throw FutureError(errc::future_already_retrieved,
                               errc_string(errc::future_already_retrieved));
        }
        return Future<T>(state_);
    }

private:
    detail::async_state_t<T> state_;
};

template<typename T>
class Promise<T&>;

template<typename U>
typename std::enable_if<!std::is_void<U>::value &&
                        !is_try<U>::value, Future<U>>::type
make_ready_future(U&& u) {
    Promise<U> p;
    p.set_value(std::move(u));
    return p.get_future();
}
    
template<typename U = void>
typename std::enable_if<std::is_void<U>::value, Future<U>>::type
make_ready_future() {
    Promise<U> p;
    p.set_value();
    return p.get_future();
}
    
template<typename U>
Future<U> make_exceptional_future(std::exception_ptr p) {
    Promise<U> prom;
    prom.set_exception(p);
    return prom.get_future();
}


} // lynx
