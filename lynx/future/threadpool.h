#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <stdexcept>
#include <utility>
#include "future.h"
#include "detail/hacky_unique_function.h"

namespace lynx {

class ThreadPool
{
public:
    using task_t = detail::hacky_unique_function<void()>;

    ThreadPool(size_t);
    ~ThreadPool();

    template<class F, class... Args>
    auto spawn(F&& f, Args&&... args)
        -> Future<typename std::result_of<F(Args...)>::type>;

private:
    template<typename F>
    void unsafe_add_task(F&& f) {
        task_t&& task = std::forward<F>(f);
        tasks_.emplace(std::move(task));
    }

private:
    // need to keep track of threads so we can join them
    std::vector<std::thread> workers_;
    // the task queue
    std::queue<task_t> tasks_;

    // synchronization
    std::mutex mtx_;
    std::condition_variable cv_;
    bool need_stop_;
};

// the constructor just launches some amount of workers
inline ThreadPool::ThreadPool(size_t threads)
    :   need_stop_(false)
{
    for(size_t i = 0;i<threads;++i)
        workers_.emplace_back(
            [this]
            {
                for(;;)
                {
                    task_t task;

                    {
                        std::unique_lock<std::mutex> lock(this->mtx_);
                        this->cv_.wait(lock,
                            [this]{ return this->need_stop_ || !this->tasks_.empty(); });
                        if(this->need_stop_ && this->tasks_.empty())
                            return;
                        task = std::move(this->tasks_.front());
                        this->tasks_.pop();
                    }

                    task();
                }
            }
        );
}

// the destructor joins all threads
inline ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(mtx_);
        need_stop_ = true;
    }
    cv_.notify_all();
    for(std::thread &worker: workers_)
        worker.join();
}

// add new work item to the pool
template<class F, class... Args>
auto ThreadPool::spawn(F&& f, Args&&... args)
    -> Future<typename std::result_of<F(Args...)>::type>
{
    using R = typename std::result_of<F(Args...)>::type;
    static_assert(!detail::is_future<R>::value, "Return Future not support!!!");

    Promise<R> p;
    auto fut = p.get_future();

    {
        std::unique_lock<std::mutex> lock(mtx_);

        // don't allow enqueueing after stopping the pool
        if(need_stop_)
            throw std::runtime_error("enqueue on stopped ThreadPool");
        
        unsafe_add_task([p=std::move(p), f=std::forward<F>(f), args...]() mutable {
            auto&& result = wrap_with_try(f, args...);
            p.set_value(std::move(result));
        });
    }
    cv_.notify_one();
    return fut;
}


template<class F, class... Args>
auto async(F&& f, Args&&... args)
    -> Future<typename std::result_of<F(Args...)>::type>
{
    static ThreadPool __pool(16);
    auto&& fut = __pool.spawn(std::forward<F>(f), std::forward<Args>(args)...);
    return std::move(fut);
}




} // lynx
