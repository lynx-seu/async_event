
#pragma once

#include <type_traits>

namespace lynx {
namespace detail {
    
// this type is copyable, but can store move-only functions
// it does so by making its copy constructor act like a move constructor
template<class T>
class copyable_value
{
public:
    using value_t = typename std::decay<T>::type;

    copyable_value(value_t&& f)
      : move_only_value_(std::move(f))
    {}

    copyable_value(copyable_value&& other)
      : move_only_value_(std::move(other.move_only_value_))
    {}

    copyable_value(const copyable_value& other)
      : move_only_value_(std::move(other.move_only_value_))
    {}

    copyable_value& operator=(const copyable_value& other)
    {
        move_only_value_ = std::move(other.move_only_value_);
        return *this;
    }

    copyable_value& operator=(copyable_value&& other)
    {
        move_only_value_ = std::move(other.move_only_value_);
        return *this;
    }

    // implicity convertion
    operator const value_t& () const & {
        return move_only_value_;
    }
    operator value_t& () & { return move_only_value_; }
    operator value_t&& () && { return std::move(move_only_value_); }


private:
    mutable value_t move_only_value_;
};

} // detail

template <class T>
detail::copyable_value<T> make_copyable(T&& t)
{
    return detail::copyable_value<T>(std::move(t));
}

} // lynx

