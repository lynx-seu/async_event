#pragma once

namespace lynx {
namespace detail {

// test if F can be called with Args type
template<typename F, typename... Args>
struct is_callable {
    // SFINAE  Check
    template<typename T,
             typename Dummy = typename std::result_of<T(Args...)>::type>
    static constexpr std::true_type
    check(std::nullptr_t dummy) {
        return std::true_type{};
    };

    template<typename Dummy>
    static constexpr std::false_type
    check(...) {
        return std::false_type{};
    };

    // the integral_constant's value
    static constexpr bool value = decltype(check<F>(nullptr))::value; 
};

} // detail
} // lynx