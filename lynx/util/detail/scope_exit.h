#pragma once

#include <utility>
#include <type_traits>

namespace lynx
{
namespace detail
{

template <class Func>
class scope_exit
{
  public:
    template <class F>
    explicit scope_exit(F &&f) noexcept
        : exit_function_(std::forward<F>(f))
    {
    }

    ~scope_exit()
    {
        exit_function_();
    }

  private:
    Func exit_function_;
};

template <class F>
scope_exit<typename std::decay<F>::type> make_scope_exit(F &&exit_function)
{
    return scope_exit<typename std::decay<F>::type>(std::forward<F>(exit_function));
}

} // namespace detail
} // namespace lynx
