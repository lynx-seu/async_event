
#pragma once

#include "helper.h"
#include <type_traits>

namespace lynx {

template<typename T>
class Future;

template<typename T>
class Promise;

template<typename T>
class Try;

namespace detail {

template<typename T>
struct is_future : std::false_type {}; 

template<typename T>
struct is_future<Future<T>>: std::true_type {};

template<typename T>
struct future_wrapper {
    using type = Future<T>;
};

template<typename T> 
struct future_wrapper<Future<T>> {
    using type = Future<T>;
};

template<typename T>
using future_wrapper_t = typename future_wrapper<T>::type;

template<typename T>
struct future_held_type;

template<typename T>
struct future_held_type<Future<T>> {
  using type = typename std::decay<T>::type;
};

template<typename T>
using future_inner_t = typename future_held_type<T>::type;

// then helper
template<typename F, typename T>
struct then_arg_ret {
    using type = typename std::result_of<F(T)>::type;
};

template<typename F>
struct then_arg_ret<F, void> {
    using type = typename std::result_of<F()>::type;
};

template<typename F>
struct then_arg_ret<F, Try<void>> {
    using type = typename std::result_of<F()>::type;
};

template<typename F, typename T>
using then_arg_ret_t = typename then_arg_ret<F, T>::type;


template<typename F, typename T>
struct callable_result {
    using type = typename std::conditional<
                            is_callable<F, T&&>::value, // if true, F(T&&) is valid
                            typename std::result_of<F(T&&)>::type, // Yes, F(T&&) is ok
                            typename std::result_of<F(T)>::type  // Resort to F(T)
                           >::type ;
    enum { is_future = is_future<type>::value };
};

template<typename F>
struct callable_result<F, typename std::enable_if<is_callable<F>::value, void>::type> {
    using type = typename std::result_of<F()>::type;

    enum { is_future = is_future<type>::value };
};
    
template<typename F>
struct callable_result<F,
            typename std::enable_if<is_callable<F, Try<void>&&>::value, void>::type> {
    using type = typename std::result_of<F(Try<void>&&)>::type;

    enum { is_future = is_future<type>::value };
};
    

template<typename F, typename T>
using then_ret_t = future_wrapper_t<typename callable_result<F, T>::type>;

} // detail
} // lynx
